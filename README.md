# Duplix
## About
Duplix is a small passion project of mine. My ultimate goal with this project
is to have a fully functioning microkernel fit with native containerization, 
as well as all the bells and whistles associated with it. On top of this lofty
goal, I also want to combine some ideas from research I encountered from
professors I had studied under in my undergraduate career, such as Barrelfish.
At this point in development (11/2/2020), I think that this project will end
up largely unfinished, but one can always remain hopeful. At best, I predict
that it will be rather inefficient compared to most modern operating systems
used in deploying high level software (i.e. web apps and other boring stuff).
With all of that pessimism aside, I also am ready to be surprised by any
promise this side project of mine may show. I think I am bringing some good
ideas into this, it's just a matter of whether or not I can implement them
well, or even functional really.

## How it works
Duplix is packaged as a full operating system with a minimal image containing
key binaries and libraries for compiling any program to run on top of the
system ABI. It is intended to conform to the latest POSIX standard (which at
the time of writing is the 2017 edition) in later stages of development.

The core philosophy behind Duplix is isolation at every level possible. This
includes kernel state, to the extent that kernels more or less operate as
kernel threads, albeit with state less oriented towards managing singular
processes and moreso geared towards managing entire "kernels". From hereon,
this document and other pieces of documentation will refer to a container's
privileged state as a "kernel", or an "environment". If other documents fail to
make a similar introduction, assume this to be the case. Similarly, Duplix
functions akin to a hypervisor. The kernel (not a kernel, but the kernel
kernel), is aware that its job is to allocate resources such as memory and I/O
devices in such a way that accesses are fair but also allow throughput, and
additionally allow for the illusion of sole ownership of what resources are
allocated. In fact, containers can even be virtualized, making Duplix operate
as both a kernel and a Type 1 paravirtualized hypervisor.

When a kernel is launched, it hinges its startup process upon a specification
referred to by documentation as an "image". This operates very similarly to
a Docker image. There are various overlays, layers of changes to the filesystem
used by the container, detailed by the image file, as well as pointers to
programs referred to as "units", which perform special functions such as the
container's file server, or managing process (init process, PID1). All of these
have special associations within the environment's control block, and as long
as a process has access to the information for an environment's control block,
it may freely communicate with said environment's units, albeit with different
results than if it were a process interior to the container.

