#include <string.h>

#include "inode.h"

int link(const char *oldpath, const char *newpath)
{
    int r;
    struct inode *oldnode;
    struct inode *newnode;
    oldnode = iopen(oldpath, 0);
    newnode = ilink(oldnode, newpath);
    r = iclose(oldnode);
    if (r < 0) {
        return r;
    }
    r = iclose(newnode);
    if (r < 0) {
        return r;
    }
    return 0;
}