#include "inode.h"

int creat(const char *pathname, mode_t mode)
{
    int r;
    struct inode *inode;
    inode = iopen(pathname, INOF_CREAT, mode);
    if (inode->i_refcount != 1) {
        /* error?s */
    }
    r = iclose(inode);
    if (r < 0) {
        return r;
    }
    return 0;
}