#include "inode.h"

#include <stdlib.h>

struct inode *iopen(const char *pathname, inoflags_t flags, ...)
{

}

int iclose(struct inode *inode)
{

}

int iflush(struct inode *inode)
{

}

int ishare(struct inode *inode, procid_t proc)
{

}

struct inode *ilink(struct inode *inode, const char *pathname)
{

}

int istat(struct inode *inode, struct stat *statbuf)
{

}