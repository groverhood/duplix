
#include "inode.h"

int stat(const char *pathname, struct stat *statbuf)
{
    int r;
    struct inode *inode;
    inode = iopen(pathname, 0);
    r = istat(inode, statbuf);
    if (r < 0) {
        return r;
    }
    r = iclose(inode);
    if (r < 0) {
        return r;
    }
    return 0;
}