#ifndef _INODE_H_
#define _INODE_H_

#include <fd.h>
#include <fs.h>
#include <pmap.h>
#include <stdint.h>
#include <stddef.h>

struct inode {
    struct fsinode {
        struct fd_shared fs_sharedfd;
        uint8_t _pad[BLKSIZ - sizeof(struct fd_shared)];
    } i_fsnode;  
    struct dev i_dev;
    size_t i_refcount;  
    blkaddr_t i_base;
    int i_flags;
};

struct hash;
extern struct hash *inode_table;

typedef int inoflags_t;

enum {
    INOF_CREAT = (1 << 0),
    INOF_LOCK  = (1 << 1)
};

struct inode *iopen(const char *pathname, inoflags_t flags, ...);
int iclose(struct inode *inode);
int iflush(struct inode *inode);
int ishare(struct inode *inode, procid_t proc);
struct inode *ilink(struct inode *inode, const char *pathname);
int istat(struct inode *inode, struct stat *statbuf);

#endif