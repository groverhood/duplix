#include <fs.h>
#include "inode.h"

int chmod(const char *pathname, mode_t mode)
{
    int r;
    struct inode *inode;
    struct fd_shared *shared;
    inode = iopen(pathname, 0);
    shared = &inode->i_fsnode.fs_sharedfd;
    spinlock_acquire(&shared->fds_lock);
    shared->fds_stat.st_mode = mode;
    spinlock_release(&shared->fds_lock);
    return 0;   
}