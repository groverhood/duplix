#ifndef _BLKADDR_H_
#define _BLKADDR_H_

#include <fd.h>
#include <dev/blkdev.h>

int blkmap(blkaddr_t ptr, blkcnt_t nblk, int flags);
blkaddr_t balloc(dev_t bdev, blkcnt_t nblk);
void freeblk(blkaddr_t ptr);

#endif