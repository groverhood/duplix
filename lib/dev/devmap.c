#include <dev.h>
#include <pmap.h>
#include <fence.h>
#include <thread.h>

/* TODO: refactor to use table lookup to avoid redundantly allocating pages */

int devmap(dev_t dev, struct dev *devobj)
{
    int r;
    int perm;
    int evtres;
    void *devva;
    struct thread_shared_mutex *devmtx;
    struct thread_sem sem;
    struct devmap_packet pkt;
    devva = pmap_node2va(pmap_alloc(get_pmap_state(), 1));
    devmtx = pmap_node2va(pmap_alloc(get_pmap_state(), 1));
    sem_init(&sem);
    pkt = (struct devmap_packet){
        PACKET_INIT(EVT_DEVMAP, &evtres, &sem),
        .dvm_devva = devva,
        .dvm_mtxva = devmtx,
        .dvm_dev = dev
    };
    r = sendevt(0, curenv->e_devs, &pkt.evt_pkt);
    if (r < 0) {
        return r;
    }
    fence;
    sem_down(&sem);
    if (evtres < 0) {
        return evtres;
    }
    devobj->dev_mtx = devmtx;
    devobj->dev_va = devva;
    devobj->dev_type = evtres;
    devobj->dev_handle = dev;
    return 0;
}