#include <ipc.h>
#include <dev.h>
#include <stdarg.h>

#include "devipc.h"

static struct ipc ipc;
static int devipc_stat(uint64_t *ret, va_list args);

typedef int (*devipcfn_t)(uint64_t *ret, va_list args);

static devipcfn_t handlers[DEVIPC_COUNT] = {
    [DEVIPC_STAT] = &devipc_stat
};

int devipc_init(void)
{
    int r;
    r = ipc_create(0, curenv->e_devs, &ipc);
    if (r < 0) {
        return r;
    }
    return 0;
}

int devipc(devipc_t ipcnum, uint64_t *ret, ...)
{
    int r;
    va_list args;
    va_start(args, ret);
    r = handlers[ipcnum](ret, args);
    va_end(args);
    return r;
}

static int devipc_stat(uint64_t *ret, va_list args)
{
    int r;
    struct devstat *statbuf;
    r = (int)ipc_wrargv(&ipc, "iz", args);
    if (r < 0) {
        return r;
    }
    r = ipc_signal(&ipc, 1);
    if (r < 0) {
        return r;
    }
    r = (int)ipc_read(ret, sizeof *ret, &ipc);
    if (r < 0) {
        return r;
    }
    statbuf = va_arg(args, struct devstat *);
    r = (int)ipc_read(statbuf, sizeof *statbuf * *ret, &ipc);
    if (r < 0) {
        return r;
    }
    return 0;
}