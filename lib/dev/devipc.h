#ifndef _DEVIPC_H_
#define _DEVIPC_H_

#include <stdint.h>

typedef int devipc_t;

enum {
    DEVIPC_STAT,
    DEVIPC_COUNT,
};

int devipc_init(void);
int devipc(devipc_t ipcnum, uint64_t *ret, ...);

#endif