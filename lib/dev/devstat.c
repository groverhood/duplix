#include <dev.h>

#include "devipc.h"

int devstat(devtype_t type, size_t *statcount, struct devstat statbuf[])
{
    int r;
    r = devipc(DEVIPC_STAT, statcount, type, *statcount, statbuf);
    if (r < 0) {
        return r;
    }
    return 0;
}