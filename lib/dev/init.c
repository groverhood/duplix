#include <dev.h>
#include <event.h>
#include "devipc.h"

int dev_init(void)
{
    int r;
    r = devipc_init();
    if (r < 0) {
        return r;
    }
    return 0;
}