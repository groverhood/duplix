#include <stdio.h>

#include "serial.h"

int init_libc(void)
{
    int r;
    r = init_libc_serial();
    if (r < 0) {
        return r;
    }

    return 0;
}