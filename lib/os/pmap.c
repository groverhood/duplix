#include <ipc.h>
#include <pmap.h>
#include <errno.h>
#include <fence.h>

void *pmap(void *destva, size_t npages, int perm)
{
    int r;
    struct pmap_state *st;
    struct pmap_node *node;
    st = get_pmap_state();
    if (destva != NULL) {
        node = pmap_lookup(st, destva);
    } else {
        node = pmap_alloc(st, npages);
        if (node == NULL) {
            return NULL;
        }
        destva = node->pn_va;
    }
    r = syscall4(SYS_PAGE_MAP, (uint64_t)destva, npages,
                 perm, (uint64_t)&destva);
    if (r < 0) {
        pmap_free(st, destva);
        return NULL;
    }
    node->pn_perm = perm;
    node->pn_mapped = 1;
    return destva;
}

int punmap(void *va, size_t npages)
{
    int r;
    struct pmap_state *st;
    struct pmap_node *node;
    if (va != NULL && npages > 0) {
        r = syscall2(SYS_PAGE_UNMAP, va, npages);
        if (r >= 0) {
            st = get_pmap_state();
            node = pmap_lookup(st, va);
            if (node != NULL) {
                pmap_free(get_pmap_state(), node);
            }
        }
    } else {
        r = -EINVAL;
    }
    return r;
}