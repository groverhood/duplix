#include <fd.h>
#include <stdint.h>

size_t genericwrite(struct fd *fd, const void *src, size_t bytes)
{
    int r;
    uint8_t *buf;
    size_t maxbytes;
    if (bytes == 0) {
        return 0;
    }
    buf = fd->fd_buf;
    maxbytes = bytes;
    while (bytes-- > 0) {
        buf[fd->fd_pos++] = *((uint8_t *)src++);
        if (fd->fd_pos == fd->fd_bufsiz) {
            r = fd->fd_flush(fd);
            fd->fd_pos = 0;
            if (r < 0) {
                return 0;
            }
        }
    }
    return (maxbytes - bytes);
}