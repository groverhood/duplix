#include <crt.h>
#include <proc.h>
#include <pmap.h>
#include <panic.h>
#include <stdlib.h>

#include "init.h"

#define ARGVLEN 32

char *process;
char *__argv[ARGVLEN];
int __argc;
struct proc *curproc;
struct env *curenv;

static void parse_argpage(void)
{
    int argc;
    char *argp;
    char *argpend;

    argc = 1;
    argp = curproc->pr_argpage;
    argpend = argp + PGSIZE;

    __argv[0] = curproc->pr_execname;
    process = curproc->pr_execname;

    while (argp != argpend && *argp != '\xff' && argc < ARGVLEN) {
        __argv[argc++] = argp;
        while (argp != argpend && *argp++ != '\0');
    }

    __argc = argc;
}

void _start(void)
{
    int r;
    ctor_t *c_it;
    ctor_t c;
    dtor_t *d_it;
    ctor_t d;
    curproc = UPROC;
    curenv = UENV;  
    r = init();
    if (r < 0) {
        curproc->pr_status = EXIT_FAILURE;
        return;
    }

    for (c_it = ctors; *c_it != NULL; ++c_it) {
        c = *c_it;
        r = c();
        if (r < 0) {
            curproc->pr_status = EXIT_FAILURE;
            return;
        }
    }

    parse_argpage();
    curproc->pr_status = main(__argc, __argv);    
    for (d_it = dtors; *d_it != NULL; ++d_it) {
        d = *d_it;
        r = d();
        if (r < 0) {
            curproc->pr_status = EXIT_FAILURE;
            return;
        }
    }
}