
#include "init.h"

int init(void)
{
    int r;
    r = arch_init();
    if (r < 0) {
        return 0;
    }

    r = init_libc();
    if (r < 0) {
        return r;
    }

    return 0;
}