#include <fs.h>
#include <fd.h>
#include <errno.h>
#include <stdio.h>
#include <stdatomic.h>

int fdnum_alloc(fdtype_t type)
{
    static atomic_int fdnums[FD_TYPE_COUNT] = {
        0,
        0,
        0,
        0,
    };
    if (type == FD_SPECIAL) {
        return -EINVAL;
    }
    return atomic_fetch_add(&fdnums[type], 1) | (type << FDNUM_BITS);
}

extern struct fd *fs_fdnum2fd(int fdnum);
extern struct fd *net_fdnum2fd(int fdnum);
extern struct fd *dev_fdnum2fd(int fdnum);
static struct fd *special_fdnum2fd(int fdnum)
{
    int index = fdnumidx(fdnum);
    if (index >= UNIQFD_COUNT) {
        return NULL;
    }
    struct fd *specialfds[] = {
        [UNIQFD_STDOUT] = stdout,
        [UNIQFD_STDIN] = stdin,
        [UNIQFD_STDERR] = stderr,
    };
    return specialfds[index];
}

typedef struct fd *(*fdnum2fd_t)(int fdnum);

struct fd *fdnum2fd(int fdnum)
{
    static fdnum2fd_t fdnum2fds[FD_TYPE_COUNT] = {
        [FD_SPECIAL] = &special_fdnum2fd,
        [FD_FS] = &fs_fdnum2fd,
        [FD_NET] = &net_fdnum2fd,
        [FD_DEV] = &dev_fdnum2fd,
    };    
    fdtype_t type = fdnumtype(fdnum);
    return fdnum2fds[type](fdnum);
}