#include <event.h>
#include <errno.h>
#include <fence.h>
#include <thread.h>

static evthandler_t handlers[EVT_TYPE_COUNT] = { 
    [0 ... EVT_TYPE_COUNT - 1] = NULL 
};

int init_events(void)
{
    curproc->pr_evthandler = (void *)&handle_event;
    return 0;
}

int evthandler(evttype_t type, evthandler_t handler)
{
    if (type < 0 || type >= EVT_TYPE_COUNT || handler == NULL) {
        return -EINVAL;
    }
    handlers[type] = handler;
    return 0;
}

void handle_event(struct trapframe *tf, struct event_packet *pkt)
{
    int r;
    int *status;
    struct thread_sem *sem;
    if (pkt->evt_type & EVT_RETURN) {
        r = pkt->evt_res;
        status = pkt->evt_status;
        sem = pkt->evt_sem;
        *status = r;
        if (sem != NULL) {
            fence;
            sem_up(sem);
        }
    } else {
        r = handlers[pkt->evt_type](pkt);
        pkt->evt_res = r;
        pkt->evt_type |= EVT_RETURN;
        sendevt(pkt->evt_sendenv, pkt->evt_sendproc, pkt);
    }
}