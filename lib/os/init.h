#ifndef _INIT_H_
#define _INIT_H_

#include <crt.h>

int init(void) __attribute__((constructor));
int init_libc(void);
/* Not marked as a ctor because init() will call it */
int arch_init(void);

#endif