#include <pmap.h>
#include <panic.h>
#include <trapframe.h>

void handle_pagefault(struct trapframe *tf, void *va)
{
    uint64_t err;
    struct pmap_node *node;
    struct pmap_state *state;
    state = get_pmap_state();
    node = pmap_lookup(state, va);
    if (node == NULL) {
        panic("attempted to access unallocated address");
    }
    err = tf->tf_err;
    if (pmap(va, node->pn_npages, node->pn_perm) == NULL) {
        panic("out of memory");
    }
}