#include <pmap.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

static struct pmap_state state;

int pmap_init(void)
{
    int r;
    state.pm_base = 0;
    state.pm_vptrt = pmap(NULL, 1, 0);
    state.pm_size = PGSIZE;
    return r;
}

struct pmap_state *get_pmap_state(void)
{
    return &state;
}

struct pmap_state *create_foreign_pmap(envid_t env, procid_t proc)
{
    /* TODO: implement this */
    return NULL;
}

static bool vpt_leafent(pmapent_t *vpt)
{
    return (*vpt & VPT_PRESENT) && (*vpt & VPT_NODE);
}

static struct pmap_node *vpt_ent2node(pmapent_t *vpt)
{
    return vpt_leafent(vpt) ? (void *)(*vpt & VPT_ADDR) : NULL;
}

static pmapent_t *vpt_entaddr(pmapent_t *vpt)
{
    return (*vpt & VPT_PRESENT) ? (void *)(*vpt & VPT_ADDR) : NULL;
}

static pmapent_t *vpt_walk_once(pmapent_t *vpt, void *va, int level)
{
    if (level <= PG_PML4 && level > PG_PGTABLE) {
        return vpt_entaddr(&vpt[PGINDEX(va, level)]);
    }
    return NULL;
}

static struct pmap_node *vpt_walk(pmapent_t *vpt, void *va)
{
    int level;
    for (level = PG_PML4; level > PG_PGTABLE && vpt != NULL && !vpt_leafent(vpt); --level) {
        vpt = vpt_walk_once(vpt, va, level);
    }
    return vpt_ent2node(vpt);
}

struct pmap_node *pmap_lookup(struct pmap_state *state, void *va)
{
    return vpt_walk(state->pm_vptrt, va);
}

struct pmap_node *pmap_reserve(struct pmap_state *state, void *va, size_t npages)
{
    int level;
    void *page;
    pmapent_t *vpt;
    pmapent_t *nxt;
    size_t allocsz;
    size_t pgindex;
    size_t szindex;
    size_t regionsz;
    struct list nodes;
    struct list_elem *el;
    struct pmap_node *node;
    size_t sizes[] = { 1, HUGESIZE / PGSIZE, GIGASIZE / PGSIZE };
    node = NULL;    
    szindex = 0;
    list_init(&nodes);
    allocsz = npages * PGSIZE;
    while (npages > 0) {
        regionsz = sizes[szindex];
        while (regionsz > npages && szindex > 0) {
            regionsz = sizes[--szindex];
        }
        vpt = state->pm_vptrt;
        for (pgindex = PG_PML4; pgindex > szindex; --pgindex) {
            nxt = vpt_walk_once(vpt, va, pgindex);
            if (nxt == NULL) {
                page = pmap(NULL, 1, 0);
                memset(page, 0, PGSIZE);
                *vpt = ((uintptr_t)page & VPT_ADDR) | VPT_PRESENT | VPT_WRITEABLE;
                nxt = page;
            }
            vpt = nxt;
        }
        node = vpt_ent2node(vpt);
        if (node == NULL) {
            node = malloc(sizeof *node);
            node->pn_va = va;
            node->pn_npages = regionsz;
            node->pn_mapped = 0;
            node->pn_perm = 0;
            node->pn_vpt = vpt;
            list_elem_init(&node->pn_elem);
            list_push(&nodes, &node->pn_elem);
        } else {
            return NULL;
        }
        npages = (npages - (regionsz / PGSIZE));
    }
    el = list_begin(&nodes);
    if (el == NULL) {
        return NULL;
    }
    return list_elem(struct pmap_node, pn_elem, el);
}

struct pmap_node *pmap_alloc(struct pmap_state *state, size_t npages)
{
    void *ptr;
    struct pmap_node *node;
    ptr = state->pm_bump;
    node = pmap_reserve(state, ptr, npages);
    if (node != NULL) {
        state->pm_bump += npages * PGSIZE;
    }
    return node;
}

int pmap_free(struct pmap_state *state, struct pmap_node *node)
{
    void *va;
    pmapent_t *vpt;
    struct list_elem *el;
    struct pmap_node *lookup;
    va = node->pn_va;
    lookup = pmap_lookup(state, va);
    if (lookup == node) {
        el = &node->pn_elem;
        while (1) {
            el = list_next(el);
            list_remove(&node->pn_elem);
            vpt = node->pn_vpt;
            *vpt = 0;
            free(node);
            if (el == NULL) {
                break;
            } else {
                node = list_elem(struct pmap_node, pn_elem, el);
            }
        }
    } else {
        return -EINVAL;
    }
    return 0;
}