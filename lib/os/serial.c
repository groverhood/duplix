#include <ipc.h>
#include <pmap.h>
#include <stdio.h>
#include <fence.h>
#include <stdlib.h>
#include <string.h>

static struct ipc ipc;
static FILE __stdout;
static FILE __stdin;
static FILE __stderr;

FILE *stdout = &__stdout;
FILE *stdin = &__stdin;
FILE *stderr = &__stderr;

enum {
    SERIAL_READ,
    SERIAL_WRITE,
};

static int init_stdout(void);
static int init_stdin(void);
static int init_stderr(void);

/* TODO: figure out setting up magical endpoints for special FILEs */
int init_libc_serial(void)
{
    int r;
    r = init_stdout();
    if (r < 0) {
        return r;
    }
    r = init_stdin();
    if (r < 0) {
        return r;
    }
    r = init_stderr();
    if (r < 0) {
        return r;
    }
    r = ipc_create(0, curproc->pr_serialep, &ipc);
    if (r < 0) {
        return r;
    }
    return 0;
}

int putchar(int chr)
{
    return fputc(chr, stdout);
}

int puts(const char *str)
{
    return fprintf(stdout, "%s\n", str);
}

int putbuf(void *buf, size_t count)
{
    return fwrite(buf, 1, count, stdout);
}

static size_t nullio(struct fd *fd, void *dst, size_t bytes)
{
    return 0;
}

static int nullio2(struct fd *fd)
{
    return 0;
}

static int stdout_flush(struct fd *fd)
{
    int r;
   
    return 0;
}

static int init_stdout(void)
{
    stdout->fd_num = 0;
    stdout->fd_pos = 0;
    stdout->fd_buf = malloc(PGSIZE);
    stdout->fd_bufsiz = PGSIZE;
    stdout->fd_mode = 0666;
    stdout->fd_shared = NULL;
    stdout->fd_read = &nullio;
    stdout->fd_write = &genericwrite;
    stdout->fd_flush = &stdout_flush;
    stdout->fd_close = &nullio2;
    return 0;
}

static size_t stdin_read(struct fd *fd, void *buf, size_t bytes)
{
    int r;
    
    return bytes;
}

static int init_stdin(void)
{
    stdin->fd_num = 1;
    stdin->fd_pos = 0;
    stdin->fd_buf = malloc(PGSIZE);
    stdin->fd_bufsiz = PGSIZE;
    stdin->fd_mode = 0666;
    stdin->fd_shared = NULL;
    stdin->fd_read = &stdin_read;
    stdin->fd_write = &nullio;
    stdin->fd_flush = &nullio2;
    stdin->fd_close = &nullio2;
    return 0;
}

static int stderr_flush(struct fd *fd)
{
    int r;
    return 0;     
}

static int init_stderr(void)
{
    stderr->fd_num = 2;
    stderr->fd_pos = 0;
    stderr->fd_buf = malloc(PGSIZE);
    stderr->fd_bufsiz = PGSIZE;
    stderr->fd_mode = 0666;
    stderr->fd_shared = NULL;
    stderr->fd_read = &nullio;
    stderr->fd_write = &genericwrite;
    stderr->fd_flush = &stderr_flush;
    stderr->fd_close = &nullio2;
    return 0;
}
