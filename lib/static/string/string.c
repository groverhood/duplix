#include <stdio.h>
#include <string.h>
#include <stdint.h>

void *memcpy(void *dst, const void *src, size_t bytes)
{
    void *origin = dst;
    switch (bytes % sizeof(uint64_t)) {
        case 7: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
        case 6: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
        case 5: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
        case 4: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
        case 3: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
        case 2: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
        case 1: *(uint8_t *)dst++ = *(uint8_t *)src++; bytes--;
    }
    while (bytes > 0) {
        *(uint64_t *)dst++ = *(uint64_t *)src++;
        bytes = bytes - 8;
    }
    return origin;
}

void *memset(void *dst, int val, size_t bytes)
{
    void *origin = dst;
    uint8_t byte = (uint8_t)val;
    switch (bytes % sizeof(uint64_t)) {
        case 7: *(uint8_t *)dst++ = byte; bytes--;
        case 6: *(uint8_t *)dst++ = byte; bytes--;
        case 5: *(uint8_t *)dst++ = byte; bytes--;
        case 4: *(uint8_t *)dst++ = byte; bytes--;
        case 3: *(uint8_t *)dst++ = byte; bytes--;
        case 2: *(uint8_t *)dst++ = byte; bytes--;
        case 1: *(uint8_t *)dst++ = byte; bytes--;
    }
    uint64_t word = ((byte << 24) | (byte << 16) | (byte << 8) | byte);
    while (bytes > 0) {
        *(uint64_t *)dst++ = val;
        bytes = bytes - 8;
    }
    return origin;
}

char *strchr(const char *s, int c)
{
    char *p;
    p = NULL;
    while (*s != 0 && *s != c) { 
        s++;
    }
    if (*s != 0) {
        p = (char *)s;
    }

    return p;
}

int strcmp(const char *s1, const char *s2)
{
    return 0;
}