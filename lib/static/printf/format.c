#include "format.h"

#include <ctype.h>
#include <stdio.h>
#include <stdint.h>

#define FMTCHR '%'
#define ALTFORMS { "", "", "0", "0x", "0x", "", "0x" }

void format_init(struct format *f, const char *format, char *dest, size_t limit)
{
    memset(f, 0, sizeof *f);
    f->fmt_buf = dest;
    f->fmt_format = format;
    f->fmt_limit = limit;
    /* Preemptively count chars we can just write to our buffer */
    format_next(f);
}

void format_next(struct format *f)
{
    const char *format = f->fmt_format;
    while (format[f->fmt_fpos] != FMTCHR && format[f->fmt_fpos] != 0) {
        f->fmt_buf[f->fmt_pos++] = format[f->fmt_fpos++];
    }
}

static int getfchar(struct format *f)
{
    return f->fmt_format[f->fmt_fpos++];
}

static int putfchar(struct format *f, int c)
{
    return (f->fmt_buf[f->fmt_pos++] = c);
}

static int format_parse(struct format *f)
{
    static fmtflags_t flagmap[] = {
        ['#'] = FFLAGS_ALTFORM,
        ['0'] = FFLAGS_ZEROPAD,
        [' '] = FFLAGS_POSSPC,
        ['+'] = FFLAGS_SIGN,
        ['-'] = FFLAGS_LJUST
    };

    static intfmt_t ifmtmap[] = {
        ['d'] = IFMT_SIGNED,
        ['i'] = IFMT_SIGNED,
        ['o'] = IFMT_OCTAL,
        ['u'] = IFMT_UNSIGNED,
        ['x'] = IFMT_HEX,
        ['X'] = IFMT_HEXCAP,
        ['c'] = IFMT_CHR,
        ['p'] = IFMT_PTR
    };

    int c;
    int width;
    const char *flags = "#0 -+";
    
    f->fmt_fpos++;
    c = getfchar(f);
    if (c == FMTCHR) {
        f->fmt_fmtchr = 1;
    } else {
        while (c != 0 && strchr(flags, c) != NULL) {
            f->fmt_flags |= flagmap[c];
            c = getfchar(f);
        }
        width = 0;
        while (c != 0 && isdigit(c)) {
            width = width * 10 + (c - '0');
            c = getfchar(f);
        }
        f->fmt_width = width;
        if (c == 'h') {
            c = getfchar(f);
            f->fmt_type = FMT_INT;
            if (c == 'h') {
                f->fmt_union.un_int.int_width = IWIDTH_BYTE;
                c = getfchar(f);
            } else {
                f->fmt_union.un_int.int_width = IWIDTH_WORD;
            }
        } else if (c == 'l') {
            c = getfchar(f);
            f->fmt_type = FMT_INT;
            if (c == 'l') {
                f->fmt_union.un_int.int_width = IWIDTH_LQUAD;
                c = getfchar(f);
            } else {
                f->fmt_union.un_int.int_width = IWIDTH_QUAD;
            }
        }
    }
    if (c == 's') {
        f->fmt_type = FMT_STR;
        f->fmt_union.un_str.str_wchar = 0;
    } else if (f->fmt_type == FMT_INT) {
        f->fmt_union.un_int.int_format = ifmtmap[c];
    } else if (strchr("diouxXcp", c) != NULL) {
        f->fmt_type = FMT_INT;
        f->fmt_union.un_int.int_format = ifmtmap[c];
        f->fmt_union.un_int.int_width = (c == 'p') ? IWIDTH_QUAD : IWIDTH_DWORD;
    } 
    return 0;
}

static size_t revint_ladj(struct format *f, int width, uintmax_t value, 
                        const char *digits, int base, fmtflags_t flags, size_t pos)
{
    static const char *altforms[IFMT_COUNT] = ALTFORMS;
    size_t r;
    int padchr;
    const char *altform;
    uintmax_t digit;
    uintmax_t rest;
    digit = (value % base);
    rest = (value / base);
    if (rest > 0) {
        r = revint_ladj(f, width - 1, rest, digits, base, flags, pos + 1);
        putfchar(f, digits[digit]);
    } else {
        r = pos;
        if (!format_end(f)) {
            if ((f->fmt_flags & FFLAGS_ALTFORM) || (f->fmt_union.un_int.int_format == IFMT_PTR)) {
                altform = altforms[f->fmt_union.un_int.int_format];
                while (*altform) {
                    width--;
                    putfchar(f, *altform++);
                    if (format_end(f)) {
                        break;
                    }
                }
            }
            padchr = (flags & FFLAGS_ZEROPAD) ? '0' : ' ';
            if (!format_end(f)) {
                putfchar(f, digits[digit]);
                while (width-- > 0 && r < f->fmt_limit) {
                    f->fmt_buf[r++] = padchr;
                }
            }
        }
    }
    return (r == pos) ? (f->fmt_pos) : r;
}

static void revint(struct format *f, int width, uintmax_t value, const char *digits, int base, fmtflags_t flags)
{
    static const char *altforms[IFMT_COUNT] = ALTFORMS;
    int padchr;
    const char *altform;
    uintmax_t digit;
    uintmax_t rest;
    digit = (value % base);
    rest = (value / base);
    if (rest > 0) {
        revint(f, width - 1, rest, digits, base, flags);
        if (!format_end(f)) {
            putfchar(f, digits[digit]);    
        }
    } else { 
        if ((f->fmt_flags & FFLAGS_ALTFORM) || (f->fmt_union.un_int.int_format == IFMT_PTR)) {
            altform = altforms[f->fmt_union.un_int.int_format];
            while (!format_end(f) && *altform) {
                width--;
                putfchar(f, *altform++);
            }
        }
        padchr = (flags & FFLAGS_ZEROPAD) ? '0' : ' ';
        while (!format_end(f) && width-- > 1) {
            putfchar(f, padchr);
        } 
        if (!format_end(f)) {
            putfchar(f, digits[digit]);
        }
    }
}

static void writeint(struct format *f, uintmax_t value, const char *digits, int base, int sign, fmtflags_t flags)
{
    int width;
    char *buf;
    size_t pos;
    pos = f->fmt_pos;
    buf = f->fmt_buf;
    width = f->fmt_width;
    if (sign) {
        intmax_t svalue = (intmax_t)value;
        if (svalue < 0) {
            buf[pos++] = '-';
            svalue = -svalue;
            width--;
        } else if (flags & FFLAGS_SIGN) {
            buf[pos++] = '+';
            width--;
        }   
    }
    if (pos == f->fmt_limit) {
        f->fmt_pos = pos;
        return;
    }
    if ((flags & FFLAGS_LJUST) == 0) {
        revint(f, width, value, digits, base, flags);
    } else {
        pos = revint_ladj(f, width, value, digits, base, flags, pos);
        f->fmt_pos = pos;
    }
}

static void format_writeint(struct format *f, va_list arglist)
{
    static int bases[IFMT_COUNT] = {
        10, 10, 8, 16, 16, 0, 16
    };
    static const char *digits_buf[IFMT_COUNT] = {
        "0123456789",
        "0123456789",
        "01234567",
        "0123456789abcdef",
        "0123456789ABCDEF",
        NULL,
        "0123456789abcdef",
    };
    if (format_end(f)) {
        return;
    }
    struct format_int *i = &f->fmt_union.un_int;
    int base = bases[i->int_format];
    fmtflags_t flags = f->fmt_flags;
    const char *digits = digits_buf[i->int_format];
    if (digits == NULL) {
        int chr = va_arg(arglist, int);
        putfchar(f, chr);
    } else if (i->int_format == IFMT_PTR) {
        void *p = va_arg(arglist, void *);
        writeint(f, (uintptr_t)p, digits, base, 0, flags);
    } else {
        switch (i->int_width) {
            case IWIDTH_BYTE:
            case IWIDTH_WORD:
            case IWIDTH_DWORD: {
                if (i->int_format == IFMT_SIGNED) {
                    writeint(f, va_arg(arglist, int), digits, base, 1, flags);
                } else {
                    writeint(f, va_arg(arglist, unsigned), digits, base, 0, flags);
                }
            } break;
            case IWIDTH_QUAD: {
                if (i->int_format == IFMT_SIGNED) {
                    writeint(f, va_arg(arglist, long), digits, base, 1, flags);
                } else {
                    writeint(f, va_arg(arglist, unsigned long), digits, base, 0, flags);
                }
            } break;
            case IWIDTH_LQUAD: {
                if (i->int_format == IFMT_SIGNED) {
                    writeint(f, va_arg(arglist, long long), digits, base, 1, flags);
                } else {
                    writeint(f, va_arg(arglist, unsigned long long), digits, base, 0, flags);
                }
            } break;
        }
    }
}

static void revstr(struct format *f, const char *s, int width, int flags, size_t pos)
{
    if (*s && pos < f->fmt_limit) {
        revstr(f, s + 1, width - 1, flags, pos + 1);
        f->fmt_buf[pos] = *s;
    } else {
        while (width-- > 0 && f->fmt_pos < f->fmt_limit) {
            putfchar(f, ' ');
        }
        f->fmt_pos = pos;
    }
}

static void format_writestr(struct format *f, va_list arglist)
{
    const char *s = va_arg(arglist, const char *);
    int width = f->fmt_width;
    int flags = f->fmt_flags;
    if (flags & FFLAGS_LJUST) {
        while (*s && f->fmt_pos < f->fmt_limit) {
            putfchar(f, *s++);
            width--;
        }
        while (width-- > 1) {
            putfchar(f, ' ');
        }
    } else {
        revstr(f, s, width, flags, f->fmt_pos);
    }   
}

static void format_writefloat(struct format *f, va_list arglist)
{

}

static void format_writearg(struct format *f, va_list arglist)
{
    typedef void (*format_writearg_t)(struct format *, va_list);
    static format_writearg_t writearg_funcs[] = {
        &format_writeint,
        &format_writestr,
        &format_writefloat
    };
    writearg_funcs[f->fmt_type - 1](f, arglist);
}

int format_write(struct format *f, va_list arglist)
{
    int i, r;
    size_t formatpos = f->fmt_fpos;
    const char *format = f->fmt_format;
    if (format[formatpos] == FMTCHR) {
        r = format_parse(f);
        if (r < 0) {
            return r;
        }
        if (f->fmt_fmtchr) {
            putfchar(f, '%');
        } else {
            format_writearg(f, arglist); 
        }
    }
    return 0;
}

int format_end(struct format *f)
{
    return (f->fmt_format[f->fmt_fpos] == 0) || (f->fmt_pos == f->fmt_limit);    
}