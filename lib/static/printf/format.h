#ifndef _FORMAT_H_
#define _FORMAT_H_

#include <stddef.h>
#include <stdarg.h>

typedef int fmttype_t;
typedef int intwidth_t;
typedef int intfmt_t;
typedef int fmtflags_t;

enum {
    IFMT_SIGNED,
    IFMT_UNSIGNED,
    IFMT_OCTAL,
    IFMT_HEX,
    IFMT_HEXCAP,
    IFMT_CHR,
    IFMT_PTR,
    IFMT_COUNT
};

enum {
    IWIDTH_BYTE,
    IWIDTH_WORD,
    IWIDTH_DWORD,
    IWIDTH_QUAD,
    IWIDTH_LQUAD,
    IWIDTH_IMAX,
    IWIDTH_SIZE,
    IWIDTH_PTRDIFF,
    IWIDTH_COUNT
};

enum {
    FFLAGS_ALTFORM = (1 << 1),
    FFLAGS_ZEROPAD = (1 << 2),
    FFLAGS_LJUST   = (1 << 3),
    FFLAGS_POSSPC  = (1 << 4),
    FFLAGS_SIGN    = (1 << 5)
};

struct format {
    /* PRINTF STATE */
    char *fmt_buf;
    const char *fmt_format;
    size_t fmt_limit;
    size_t fmt_pos;
    size_t fmt_fpos;
    size_t fmt_regwrcnt;

    /* FORMAT ARG */
    fmttype_t fmt_type;
    int fmt_storelen;
    int fmt_width;
    int fmt_fmtchr;
    fmtflags_t fmt_flags;
    union format_union {
        struct format_int {
            intwidth_t int_width;
            intfmt_t int_format;
        } un_int;
        struct format_str {
            int str_wchar;
            
        } un_str;
        struct format_float {

        } un_float;
    } fmt_union;
};

enum {
    FMT_INT = 1,
    FMT_STR,
    FMT_FLOAT
};

void format_init(struct format *f, const char *format, char *dest, size_t limit);
void format_next(struct format *f);
int format_write(struct format *f, va_list arglist);
int format_end(struct format *f);

#endif