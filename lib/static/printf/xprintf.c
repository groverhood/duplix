/**
 *  Generic *printf implementations, all of which reduce to calls to
 *  vsnprintf().
 **/

#include <stdio.h>

#define PBUFSIZ 256

int printf(const char *format, ...)
{
    int res;
    va_list args;
    va_start(args, format);
    res = vprintf(format, args);
    va_end(args);
    return res;
}

int vprintf(const char *format, va_list args)
{
    int res, i;
    static char printbuf[PBUFSIZ];
    res = vsnprintf(printbuf, PBUFSIZ, format, args);
    if (res < 0) {
        return res;
    }
    res = putbuf(printbuf, res);
    return res;
}

int vsprintf(char *dest, const char *format, va_list args)
{
    return vsnprintf(dest, INT32_MAX, format, args);    
}

int sprintf(char *dest, const char *format, ...)
{
    int res;
    va_list args;
    va_start(args, format);
    res = vsnprintf(dest, INT32_MAX, format, args);
    va_end(args);
    return res;
}

int snprintf(char *dest, size_t size, const char *format, ...)
{
    int res;
    va_list args;
    va_start(args, format);
    res = vsnprintf(dest, size, format, args);
    va_end(args);
    return res;
}

#include "format.h"

int vsnprintf(char *dest, size_t size, const char *format, va_list args)
{
    int r;
    struct format f;
    for (format_init(&f, format, dest, size); !format_end(&f); format_next(&f)) {
        r = format_write(&f, args);
        if (r < 0) {
            return r;
        }     
    }
    return f.fmt_pos;
}