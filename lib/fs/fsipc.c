#include <fd.h>
#include <fs.h>
#include <ipc.h>
#include <stdarg.h>

#include "fd.h"
#include "file.h"
#include "fsipc.h"

typedef int (*fsipcfn_t)(uint64_t *ret, va_list args);

static int fsipc_open(uint64_t *ret, va_list args);
static int fsipc_creat(uint64_t *ret, va_list args);
static int fsipc_unlink(uint64_t *ret, va_list args);
static int fsipc_chmod(uint64_t *ret, va_list args);
static int fsipc_stat(uint64_t *ret, va_list args);

static struct ipc ipc;
static fsipcfn_t handlers[FSIPC_COUNT] = {
    [FSIPC_CREAT] = &fsipc_creat,
    [FSIPC_UNLINK] = &fsipc_unlink,
    [FSIPC_CHMOD] = &fsipc_chmod,
    [FSIPC_STAT] = &fsipc_stat
};

int fsipc_init(void)
{
    int r;
    r = ipc_create(0, curenv->e_fs, &ipc);
    if (r < 0) {
        return r;
    }
    return 0;
}

int fsipc(int ipcnum, uint64_t *ret, ...)
{
    int r;
    va_list args;
    va_start(args, ret);
    r = handlers[ipcnum](ret, args);
    va_end(args);
    if (r < 0) {
        return r;
    }
    return 0;
}

static int fsipc_open(uint64_t *ret, va_list args)
{
    int r;
    struct fd_shared *buf;
    
    r = (int)ipc_wrargv(&ipc, "sii", args);
    if (r < 0) {
        return r;
    }

}

static int fsipc_creat(uint64_t *ret, va_list args)
{

}

static int fsipc_unlink(uint64_t *ret, va_list args)
{

}

static int fsipc_chmod(uint64_t *ret, va_list args)
{

}

static int fsipc_stat(uint64_t *ret, va_list args)
{

}