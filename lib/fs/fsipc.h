#ifndef _FSIPC_H_
#define _FSIPC_H_

#include <stdint.h>

int fsipc_init(void);

int fsipc(int ipcnum, uint64_t *ret, ...);

#endif