#include <fd.h>
#include <stdlib.h>
#include <string.h>

#include "fd.h"
#include "fsipc.h"

struct fd *fs_fdopen(const char *pathname, int flags, mode_t mode)
{
    int r;
    uintptr_t shared;
    struct fd *usrfd;
    if (flags & (O_CREAT | O_TMPFILE)) {
        r = fsipc(FSIPC_OPEN3, &shared, pathname, flags, mode);
    } else {
        r = fsipc(FSIPC_OPEN3, &shared, pathname, flags);
    }
    if (r < 0) {
        return NULL;
    }
    usrfd = malloc(sizeof *usrfd);
    if (usrfd == NULL) {
        return NULL;
    }
    usrfd->fd_shared = (struct fd_shared *)shared;
    /* TODO: Use stat to identify dev files */
    usrfd->fd_num = fdnum_alloc(FD_FS);
    usrfd->fd_buf = malloc(IOBUFSIZ);
    usrfd->fd_bufsiz = IOBUFSIZ;
    usrfd->fd_pos = 0;
    usrfd->fd_read = &fs_fdread;
    usrfd->fd_write = &fs_fdwrite;
    usrfd->fd_close = &fs_fdclose;
    usrfd->fd_flush = &fs_fdflush;
    return usrfd;
}

int fs_fdclose(struct fd *fd)
{

}

static inline size_t fs_filesz(struct fd_shared *fd)
{
    return atomic_load(&fd->fds_stat.st_size);
}

size_t fs_fdread(struct fd *fd, void *buf, size_t size)
{
    size_t pos;
    size_t ofs;
    size_t diff;
    size_t nread;
    size_t blksize;
    blkaddr_t base;
    struct fd_shared *shared;
    ofs = fd->fd_ofs;
    shared = fd->fd_shared;
    blksize = blk_size(&fd->fd_dev);
    while (size > 0 && ofs < fs_filesz(shared)) {
        pos = fd->fd_pos;
        nread = fd->fd_nread;
        while (nread > 0 && size > 0 && pos < fd->fd_bufsiz) {
            *(uint8_t *)buf++ = fd->fd_buf[pos++];
            nread--;
            size--;
            ofs++;
        }
        fd->fd_pos = pos;
        fd->fd_nread = nread;
        if (size == 0) {
            break;
        }
        if (ofs < FDEXTSZ) {
            spinlock_acquire(&shared->fds_lock);
            while (ofs < FDEXTSZ && size > 0) {
                *(uint8_t *)buf++ = shared->fds_data[ofs++];
                size--;
            }
            spinlock_release(&shared->fds_lock);
        } else {
            shmutex_lock(fd->fd_dev.dev_mtx);
            blk_read(&fd->fd_dev, ofs / blksize, fd->fd_buf, IOBUFSIZ / BLKSIZ);
            shmutex_unlock(fd->fd_dev.dev_mtx);
            fd->fd_pos = ofs % blksize;
            fd->fd_nread = blksize - fd->fd_pos;            
        }
    }
    diff = ofs - fd->fd_ofs;
    fd->fd_ofs = ofs;
    return diff;
}

size_t fs_fdwrite(struct fd *fd, const void *buf, size_t size)
{
    size_t ofs;
    size_t writecnt;
    struct fd_shared *shared;
    ofs = fd->fd_ofs;
    shared = fd->fd_shared;
    if (size == 0) {
        return 0;
    }
    if (ofs < FDEXTSZ) {
        spinlock_acquire(&shared->fds_lock);
        do {
            shared->fds_data[ofs++] = *(const uint8_t *)buf++;
            size--;
        } while (ofs < FDEXTSZ);
        spinlock_release(&shared->fds_lock);
        writecnt = ofs - fd->fd_ofs;
    } else {
        writecnt = 0;
    }
    if (size > 0) {
        writecnt += genericwrite(fd, buf, size);
    }
    return writecnt;
}

int fs_fdflush(struct fd *fd)
{
}