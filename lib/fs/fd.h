#ifndef _FSFD_H_
#define _FSFD_H_

#include <fs.h>
#include <fd.h>
#include <stddef.h>

enum { IOBUFSIZ = (128 * 1024) };

struct fd *fs_fdnum2fd(int fdnum);
struct fd *fs_fdopen(const char *pathname, int flags, mode_t mode);
int fs_fdclose(struct fd *fd);
int fs_fdflush(struct fd *fd);

size_t fs_fdread(struct fd *fd, void *buf, size_t size);
size_t fs_fdwrite(struct fd *fd, const void *buf, size_t size);

#endif