#include <fs.h>
#include <ipc.h>
#include <env.h>

#include "file.h"
#include "fsipc.h"

int init_libfs(void)
{
    int r;
    r = fsipc_init();
    if (r < 0) {
        return r;
    }
    return 0;
}