#include <fs.h>
#include <string.h>

#include "fd.h"
#include "file.h"
#include "fsipc.h"

int open3(const char *pathname, int flags, mode_t mode)
{
    struct fd *fd = fs_fdopen(pathname, flags, mode);
    return fd->fd_num;
}

int open2(const char *pathname, int flags)
{
    struct fd *fd = fs_fdopen(pathname, flags, 0);
    return fd->fd_num;
}

int creat(const char *pathname, mode_t mode)
{
    int r;
    uint64_t rpc_r;
    r = fsipc(FSIPC_CREAT, &rpc_r, pathname, mode);
    if (r < 0) {
        return r;
    }
    if ((int)rpc_r < 0) {
        return (int)rpc_r;
    }
    return 0;
}

int unlink(const char *pathname)
{
    int r;
    uint64_t rpc_r;
    r = fsipc(FSIPC_UNLINK, &rpc_r, pathname);
    if (r < 0) {
        return r;
    }
    if ((int)rpc_r < 0) {
        return (int)rpc_r;
    }
    return 0;
}

int chmod(const char *pathname, mode_t mode)
{
    int r;
    uint64_t rpc_r;
    r = fsipc(FSIPC_CHMOD, &rpc_r, pathname, mode);
    if (r < 0) {
        return r;
    }
    if ((int)rpc_r < 0) {
        return (int)rpc_r;
    }
    return 0;
}

int stat(const char *filename, struct stat *out_stat)
{
    int r;
    uint64_t rpc_r;
    r = fsipc(FSIPC_CHMOD, &rpc_r, filename, out_stat);
    if (r < 0) {
        return r;
    }
    if ((int)rpc_r < 0) {
        return (int)rpc_r;
    }
    return 0;
}