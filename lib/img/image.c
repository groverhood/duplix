
#include <fs.h>
#include <image.h>

extern void *malloc(size_t bytes);
extern void free(void *ptr);

int image(const char *file, struct imagehdr **out_im)
{
    uint8_t *imgbuf;
    struct stat st;
    int stream = open(file, 0);
    if (stream < 0) {
        return stream;
    }

    int status = fstat(stream, &st);
    if (status < 0) {
        return status;
    }

    // TODO: Read size from stat struct and read into buffer,
    imgbuf = malloc(st.st_size);
    status = (int)read(stream, imgbuf, st.st_size);
    if (status < 0) {
        free(imgbuf);
        return status;
    }

    status = image2(imgbuf, out_im);
    if (status < 0) {
        free(imgbuf);
        return status;
    }    

    return 0;
}