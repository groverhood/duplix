#include <image.h>

extern void free(void *ptr);

void freeimg(struct imagehdr *img)
{
    free(img);
}