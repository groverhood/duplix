#include <pmap.h>
#include <mmap.h>
#include <errno.h>
#include <stdio.h>
#include <stdbool.h>

#ifdef __amd64__
// TODO: replace this with something more idiomatic
#include "arch/amd64/paging.h"
#include "arch/amd64/multiboot.h"
#endif

#include "pgalloc.h"

/* Tag structure for free list */
struct page {
    struct list_elem pg_elem;
    size_t pg_npages;
};

static struct list free_list;
extern size_t phys_ram_size;


static void compact(void);

int pgalloc_init(struct memorymap *mmap, size_t mmapsz, size_t entsz)
{
    while (mmapsz > 0) {
        if (mmap->mm_type == MM_TYPE_LOADERDATA || mmap->mm_type == MM_TYPE_BOOTSRVCODE 
            || mmap->mm_type == MM_TYPE_BOOTSRVDATA || mmap->mm_type == MM_TYPE_RTSRVDATA
            || mmap->mm_type == MM_TYPE_CONVENTIONAL) {
            void *kva = paging_pa2kva(mmap->mm_phys);
            struct page *pg = kva;
            list_elem_init(&pg->pg_elem);
            list_push(&free_list, &pg->pg_elem);
            pg->pg_npages = mmap->mm_npages;
        }
        mmap = mm_next(mmap, entsz);
        mmapsz = mmapsz - entsz;
    }
    compact();
    return 0;
}

void *page_alloc(size_t npages, int flags)
{
    struct page *p;
    struct page *new_p;
    struct list_elem *el;
    p = NULL;
    for (el = list_begin(&free_list); el != list_end(&free_list); el = list_next(el)) {
        p = list_elem(struct page, pg_elem, el);
        if (p->pg_npages == npages) {
            list_delete(&free_list, el);
            break; 
        } else if (p->pg_npages > npages) {
            new_p = (struct page *)((uint8_t *)p + npages * PGSIZE);
            new_p->pg_npages = p->pg_npages - npages;
            list_delete(&free_list, el);
            list_push(&free_list, &new_p->pg_elem);
            break;
        } else {
            p = NULL;
        }       
    }
    if (p != NULL) {
        page_handle_flags(p, npages, flags);
    }
    return (void *)p;
}

int page_handle_flags(void *base, size_t npages, int flags)
{
    uint8_t *buf;
    size_t bufsz;
    if (base == NULL) {
        return -EINVAL;
    }
    if (flags & PGALLOC_ZERO) {
        buf = (uint8_t *)base;
        bufsz = PGSIZE * npages;
        while (bufsz-- > 0) {
            *buf++ = 0;
        }
    }
    /* TODO: handle future flags */
    return 0;
}

void page_free(void *page, size_t npages)
{
    /* TODO: handle fragmentation */
    struct page *p = page;
    p->pg_npages = npages;
    list_push(&free_list, &p->pg_elem);   
}

static void compact(void)
{

}

void print_freelist(void)
{
    size_t npages;
    struct list_elem *e;
    npages = 0;
    for (e = list_begin(&free_list); e != list_end(&free_list); e = list_next(e)) {
        struct page *pg = list_elem(struct page, pg_elem, e);
        printf("[%p, %p) [%6lu PAGES]\n", pg, (uint8_t *)pg + pg->pg_npages * PGSIZE, pg->pg_npages);
        npages += pg->pg_npages;
    }
    printf("%lu PAGES TOTAL (~%luK)\n", npages, (npages * 4) / 1024);
}