#ifndef _KSYSCALL_H_
#define _KSYSCALL_H_

#include <trapframe.h>
#include <stdint.h>
#include <syscn.h>

struct kenv;

void handle_syscall(struct trapframe *tf);

int init_syscalls(void);

int ksyscall(struct kenv *ctx, syscnum_t callnum);

#endif