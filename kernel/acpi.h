#ifndef _ACPI_H_
#define _ACPI_H_

#include "paging.h"

#include <stdint.h>
#include <static_assert.h>

struct acpiv1_rsdp {
    char r_sig[8];
    uint8_t r_checksum;
    char r_oemid[6];
    uint8_t r_revision;
    uint32_t r_physaddr;
};

struct acpiv2_rsdp {
    char r_sig[8];
    uint8_t r_chksum;
    char r_oemid[6];
    uint8_t r_revision;
    uint32_t r_rsdtpaddr;
    uint32_t r_length;
    uint64_t r_xsdtpaddr;
    uint8_t r_extchksum;
};

struct rsdt {
    char r_sig[4];
    uint32_t r_length;
    uint8_t r_revision;
    uint8_t r_chksum;
    char r_oemid[6];
    uint64_t r_oemtableid;
    uint32_t r_oemrevision;
    uint32_t r_creatorid;
    uint32_t r_paddrs[0];
};

struct xsdt {
    char x_sig[4];
    uint32_t x_length;
    uint8_t x_revision;
    uint8_t x_chksum;
    char x_oemid[6];
    uint64_t x_oemtableid;
    uint32_t x_oemrevision;
    uint32_t x_creatorid;
    uint64_t x_paddrs[0];
};

int init_rsdt(struct rsdt *rsdt);

static int init_acpiv1(struct acpiv1_rsdp *rsdp)
{
    /* TODO: implement this */
}

int init_xsdt(struct xsdt *xsdt);

static int init_acpiv2(struct acpiv2_rsdp *rsdp)
{
    int r;
    if (rsdp->r_xsdtpaddr != 0) {
        r = init_xsdt(paging_pa2kva(rsdp->r_xsdtpaddr));
    } else {
        r = init_rsdt(paging_pa2kva(rsdp->r_rsdtpaddr));
    }
    return (r < 0) ? r : 0;
}


#endif