#include <errno.h>
#include <stdio.h>

#include "kenv.h"
#include "pgalloc.h"

#define NENV (PGSIZE / sizeof(struct kenv *))
struct kenv **kernels;

struct env *curenv = NULL;
struct proc *curproc = NULL;
struct kenv *bspenv = NULL;

struct list sched_list;
struct lock kernlock;

static envid_t id_alloc(void);

struct kenv_initarg {
    void *arg_procbuf;
    size_t arg_procbufsz;
    void *arg_kernstk;
    size_t arg_kernstksz;
    envtype_t arg_etype;
    envid_t arg_id;
};

static void init_kenv(struct kenv *e, struct kenv_initarg *arg)
{
    struct env *env = &e->ke_env;
    env->e_next = -1;
    env->e_self = arg->arg_id;
    env->e_type = arg->arg_etype;
    atomic_init(&env->e_trapped, false);
    env->e_curproc = 1;
    env->e_fs = -1;
    env->e_ns = -1;
    env->e_dyns = -1;
    lock_init(&e->ke_lock);
    envmem_init(&e->ke_mem);
    list_elem_init(&e->ke_elem);
    e->ke_unalloc = 0;
    e->ke_procbuf = arg->arg_procbuf;
    e->ke_procbufsz = arg->arg_procbufsz;
    e->ke_kernstk = arg->arg_kernstk;
    e->ke_kernstksz = arg->arg_kernstksz;
}

/* Intialize kernel env system */
int kenv_init(void)
{
    struct kenv_initarg arg = {
        .arg_kernstk = page_alloc(1, PGALLOC_ZERO),
        .arg_kernstksz = PGSIZE,
        .arg_procbuf = page_alloc(2, PGALLOC_ZERO),
        .arg_procbufsz = PGSIZE * 2,
        .arg_id = id_alloc(),
        .arg_etype = ENV_TYPE_HOST
    };

    kernels = page_alloc(1, PGALLOC_ZERO);
    lock_init(&kernlock);
    bspenv = page_alloc(KENVPAGES, 0);
    init_kenv(bspenv, &arg);
    curenv = (struct env *)bspenv;
    list_init(&sched_list);
    return 0;
}

/* Create a kernel */
struct kenv *kenv_create(envtype_t type)
{
    envid_t i;
    struct kenv *e;
    struct kenv_initarg arg;
    lockbsp();
    i = id_alloc();
    unlockbsp();
    if (i < 0) {
        return NULL;
    }
    arg.arg_id = i;
    e = kernels[i];
    if (e == NULL) {
        e = page_alloc(KENVPAGES, 0);
        kernels[i] = e;
    }
    arg.arg_procbuf = page_alloc(2, PGALLOC_ZERO);
    arg.arg_procbufsz = 2 * PGSIZE;
    arg.arg_kernstk = page_alloc(1, PGALLOC_ZERO);
    arg.arg_kernstksz = PGSIZE;
    arg.arg_etype = type;
    init_kenv(e, &arg);
    return e;
}

/* Kill a kernel */
int kenv_kill(struct kenv *env)
{
    if (env == NULL || env->ke_unalloc) {
        return -EINVAL;
    }
    env->ke_unalloc = 1;
    list_remove(&env->ke_elem);
    return 0;
}

int kenv_sched(struct kenv *env)
{
    return 0;
}

void kenv_yield(void)
{

}

void kenv_switch(struct kenv *next)
{

}

static envid_t id_alloc(void)
{
    envid_t id;
    for (id = 0; id < NENV; ++id) {
        struct kenv *env = kernels[id];
        if (env == NULL || env->ke_unalloc) {
            break;
        }
    }
    /* TODO: return errno if  */
    return id;
}