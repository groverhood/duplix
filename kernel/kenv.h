#ifndef _KENV_H_
#define _KENV_H_

#include <env.h>
#include <pmap.h>
#include <list.h>
#include <sysarg.h>
#include <stdint.h>
#include <stddef.h>
#include <spinlock.h>
#include <static_assert.h>

#include "lock.h"
#include "pgalloc.h"

struct cap;
struct proc;

/* One big kernel lock */
extern struct lock kernlock;

static inline void lockbsp(void)
{
    lock_acquire(&kernlock);
}

static inline void unlockbsp(void)
{
    lock_release(&kernlock);
}

/* This is the only non-machine state that isn't stored in a kenv. */
extern struct kenv **kernels;

/**
 *  Kernel environment data structure. Provides important structures
 *  for managing environments from userland and for ensuring kernel
 *  isolation between environments.
 */
struct kenv {
    struct env ke_env;           // >>> Shared user/kernel env structure
    uint8_t _pad0[PGSIZE - sizeof(struct env)];

    /* Inaccessible to the userspace */
    struct envmem ke_mem;        // >>> Env memory used for processes
    struct proc **ke_procbuf;
    size_t ke_procbufsz;         // >>> Process buffer size
    struct list_elem ke_elem;    // >>> Scheduling elem
    struct lock ke_lock;
    uint8_t *ke_restorestk;      // >>> Stack to restore from a system call        
    uint8_t *ke_kernstk;         // >>> System call stack
    size_t ke_kernstksz;
    int ke_unalloc;
} ;

#define KENVPAGES (((sizeof(struct kenv) + PGSIZE - 1) / PGSIZE))
#define KENVSIZE (PGSIZE * KENVPAGES)

static_assert(offsetof(struct kenv, ke_mem) == PGSIZE, "Kernel env struct not properly aligned");
static_assert(KENVSIZE % PGSIZE == 0, "Kernel env struct is not page aligned");

/* Special env spun up by the kernel that has special permissions
   for interfacing with hardware. Initialized as curenv in kenv_init(). */
extern struct kenv *bspenv;


/* Intialize kernel env system */
int kenv_init(void);
/* Create a kernel */
struct kenv *kenv_create(envtype_t type);
int kenv_sched(struct kenv *env);
void kenv_yield(void);
/* Switch to a different kernel */
void kenv_switch(struct kenv *next);
/* Kill a kernel */
int kenv_kill(struct kenv *env);

static inline void kenv_lock(struct kenv *env)
{
    lock_acquire(&env->ke_lock);   
}

static inline void kenv_unlock(struct kenv *env)
{
    lock_release(&env->ke_lock);
}

#endif