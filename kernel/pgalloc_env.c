#include <pmap.h>

#include "kenv.h"
#include "pgalloc.h"

void *env_page_alloc(struct envmem *emem, size_t npages, int flags)
{
    void *page;
    struct list_elem *e;
    struct env_page *ep;
    struct env_page *ep_new;
    if (emem->em_freepages < npages) {
        lockbsp();        
        page = page_alloc(npages, flags);
        unlockbsp();
        emem->em_pages += npages;
    } else {
        ep = NULL;
        for (e = list_begin(&emem->em_freelist); e != list_end(&emem->em_freelist); e = list_next(e)) {
            ep = list_elem(struct env_page, ep_elem, e);
            if (ep->ep_pages == npages) {
                page = ep;
                list_remove(e);
                break;
            } else if (ep->ep_pages > npages) {
                ep_new = (struct env_page *)((uint8_t *)ep + ep->ep_pages * PGSIZE);
                ep_new->ep_pages = ep->ep_pages - npages;
                list_elem_init(&ep_new->ep_elem);
                page = ep;
                list_remove(e);
                list_push(&emem->em_freelist, &ep_new->ep_elem);
                break;
            } else {
                ep = NULL;
            }
        }
        page = ep;
    }
    if (page != NULL) {
        page_handle_flags(page, npages, flags);
    }
    return page;
}

void env_page_free(struct envmem *emem, void *page, size_t npages)
{
    /* TODO: handle compaction */
    struct env_page *ep;
    if (emem == NULL || page == NULL || npages == 0) {
        return;
    }

    ep = page;
    ep->ep_pages = npages;
    list_elem_init(&ep->ep_elem);
    list_push(&emem->em_freelist, &ep->ep_elem);
}

void *env_page_release(struct envmem *emem, size_t npages)
{
    /* Catch this case so that we don't allocate from the global
       page allocator */
    if (emem == NULL || npages == 0 || emem->em_freepages < npages) {
        return NULL;
    }

    void *page = env_page_alloc(emem, npages, 0);
    if (page != NULL) {
        emem->em_pages -= npages;
    }
    return page;    
}