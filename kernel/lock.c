#include "kenv.h"
#include "lock.h"

#include <stdio.h>

void lock_init(struct lock *lock)
{
    spinlock_init(&lock->l_guard);
    spinlock_acquire(&lock->l_guard);
    list_init(&lock->l_queue);
    lock->l_held = false;
    spinlock_release(&lock->l_guard);
}

void lock_acquire(struct lock *lock)
{
    spinlock_acquire(&lock->l_guard);
    if (lock->l_held) {
        list_push(&lock->l_queue, &((struct kenv *)curenv)->ke_elem);
        spinlock_release(&lock->l_guard);
        kenv_yield();
    } else {
        lock->l_held = true;
        spinlock_release(&lock->l_guard);
    }
}

void lock_release(struct lock *lock)
{
    struct list_elem *e;
    spinlock_acquire(&lock->l_guard);
    if (list_null(&lock->l_queue)) {
        lock->l_held = false;
    } else {
        list_pop(&lock->l_queue);
    }
    spinlock_release(&lock->l_guard);
}
