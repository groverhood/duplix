#ifndef _SCHED_H_
#define _SCHED_H_

#include <stdint.h>

struct trapframe;

extern int64_t kepoch;

int sched_init(void);
void sched_yield(void);
void handle_timer_interrupt(struct trapframe *tf);

#endif