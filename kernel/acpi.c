#include "acpi.h"

#include <string.h>

typedef int acpi_tabfn_t(void *tab_);

struct acpi_tabdesc {
    char at_sig[4];
    acpi_tabfn_t *at_fn;
};

#define NUM_TABDESCS 32
static struct acpi_tabdesc tabdescs[NUM_TABDESCS];

int init_rsdt(struct rsdt *rsdt)
{
    int r = 0;
    char *sig;
    size_t len = (rsdt->r_length - sizeof *rsdt) / sizeof(uint32_t);
    size_t i, j;
    uint32_t paddr, *paddrs;

    paddrs = (uint32_t *)(rsdt + 1);

    for (i = 0; i < len; ++i) {
        paddr = paddrs[i];
        sig = paging_pa2kva(paddr);
        
        for (j = 0; j < NUM_TABDESCS; ++j) {
            if (tabdescs[i].at_fn == NULL) {
                continue;
            }

            if (!strcmp(tabdescs[i].at_sig, sig)) {
                r = tabdescs[i].at_fn((void *)sig);
                break;
            }
        }
    }
    return r;
}

int init_xsdt(struct xsdt *xsdt)
{
    int r = 0;

    char *sig;
    size_t len = (xsdt->x_length - sizeof *xsdt) / sizeof(uintptr_t);
    size_t i, j;
    uintptr_t paddr, *paddrs;

    paddrs = (uintptr_t *)(xsdt + 1);

    for (i = 0; i < len; ++i) {
        paddr = paddrs[i];
        sig = paging_pa2kva(paddr);
        
        for (j = 0; j < NUM_TABDESCS; ++j) {
            if (tabdescs[i].at_fn == NULL) {
                continue;
            }

            if (!strcmp(tabdescs[i].at_sig, sig)) {
                r = tabdescs[i].at_fn((void *)sig);
                break;
            }
        }
    }

    return r;
}