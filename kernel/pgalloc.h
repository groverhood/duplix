#ifndef _PGALLOC_H_
#define _PGALLOC_H_

#include <list.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define PGALLOC_ZERO 1
/* Allocate a fragmented list of pages if a continuous allocation
   isn't possible. */
#define PGALLOC_FRAG 2

struct memorymap;

int pgalloc_init(struct memorymap *mmap, size_t mmapsz, size_t entsz);

void *page_alloc(size_t npages, int flags);
void page_free(void *page, size_t npages);
int page_handle_flags(void *base, size_t npages, int flags);

void print_freelist(void);

struct env_page {    
    struct list_elem ep_elem;
    size_t ep_pages;
};

static inline int envpgcmp(struct env_page *left, struct env_page *right)
{
    int cmp;
    if (left == right) {
        cmp = 0;
    } else if ((uintptr_t)left < (uintptr_t)right) {
        cmp = -1;
    } else {
        cmp = 1;
    }
    return cmp;
}

struct envmem {
    struct list em_freelist;
    size_t em_pages;
    size_t em_freepages;
};

static inline void envmem_init(struct envmem *emem)
{
    list_init(&emem->em_freelist);
    emem->em_pages = 0;
    emem->em_freepages = 0;
}

void *env_page_alloc(struct envmem *emem, size_t npages, int flags);
void env_page_free(struct envmem *emem, void *page, size_t npages);
void *env_page_release(struct envmem *emem, size_t npages);

#endif