#include <pmap.h>
#include <errno.h>
#include <stdbool.h>

#include "asm.h"
#include "paging.h"
#include "../../pgalloc.h"


void *phys_ram_base;
size_t phys_ram_size;



int paging_uva2kva(pml4e_t *pml4, void *uva, void **kva)
{
    int is_page;
    pdpte_t *pdpt;
    pgdire_t *pgdir;
    pte_t *pt;

    if ((uintptr_t)uva >= UTOP) {
        return -EINVAL;
    }

    if (paging_walk_pml4(pml4, uva, (void **)&pdpt) < 0) {
        return -EINVAL;
    }

    is_page = paging_walk_pdpt(pdpt, uva, (void **)&pgdir);
    if (is_page < 0) {
        return -EINVAL;
    } else if (is_page) {
        *kva = pgdir;
        return 0;
    }

    is_page = paging_walk_pgdir(pgdir, uva, (void **)&pt);
    if (is_page < 0) {
        return -EINVAL;
    } else if (is_page) {
        *kva = pt;
        return 0;
    }

    if (paging_walk_pt(pt, uva, kva) < 0) {
        return -EINVAL;
    }

    return 0;
}

int paging_map_pdpt2pml4(pml4e_t *pml4, int index, pdpte_t *pdpt, int perm)
{
    /* TODO: Check flags */
    if (0) {
        return -EINVAL;
    }

    uintptr_t phys_addr = paging_kva2pa(pdpt);
    pml4[index] = (phys_addr & PHYSMASK) | perm;
    return 0;
}

int paging_map_pgdir2pdpt(pdpte_t *pdpt, int index, pgdire_t *pgdir, int perm)
{
    /* TODO: Check flags */
    if (0) {
        return -EINVAL;
    }

    uintptr_t phys_addr = paging_kva2pa(pgdir);
    pdpt[index] = (phys_addr & PHYSMASK) | perm;
    return 0;
}

int paging_map_page2pdpt(pdpte_t *pdpt, int index, void *gigapage, int perm)
{
    /* TODO: Check flags */
    if (0) {
        return -EINVAL;
    }

    uintptr_t phys_addr = paging_kva2pa(gigapage);
    pdpt[index] = (phys_addr & PHYSMASK) | perm;
    return 0;
}

int paging_map_pt2pgdir(pgdire_t *pagedir, int index, pte_t *pt, int perm)
{
    /* TODO: Check flags */
    if (0) {
        return -EINVAL;
    }

    uintptr_t phys_addr = paging_kva2pa(pt);
    pagedir[index] = (phys_addr & PHYSMASK) | perm;
    return 0;
}

int paging_map_page2pgdir(pgdire_t *pagedir, int index, void *hugepage, int perm)
{
    /* TODO: Check flags */
    if (0) {
        return -EINVAL;
    }

    uintptr_t phys_addr = paging_kva2pa(hugepage);
    pagedir[index] = (phys_addr & PHYSMASK) | perm;
    return 0;
}

int paging_map_page2pt(pte_t *pt, int index, void *page, int perm)
{
    /* TODO: Check flags */
    if (0) {
        return -EINVAL;
    }

    uintptr_t phys_addr = paging_kva2pa(page);
    pt[index] = (phys_addr & PHYSMASK) | perm;
    return 0;
}

int paging_walk_pml4(pml4e_t *pml4, void *va, void **out_addr)
{
    if (pml4 == NULL || va == NULL) {
        return -EINVAL;
    }

    int index = PGINDEX(va, 3);
    uintptr_t pa = (pml4[index] & PHYSMASK);
    *out_addr = paging_pa2kva(pa);
    return 0;
}

int paging_walk_pdpt(pdpte_t *pdpt, void *va, void **out_addr)
{
    if (pdpt == NULL || va == NULL) {
        return -EINVAL;
    }

    int index = PGINDEX(va, 2);
    uintptr_t pa = (pdpt[index] & PHYSMASK);
    *out_addr = paging_pa2kva(pa);
    return 0;
}

int paging_walk_pgdir(pgdire_t *pgdir, void *va, void **out_addr)
{
    if (pgdir == NULL || va == NULL) {
        return -EINVAL;
    }

    int index = PGINDEX(va, 1);
    uintptr_t pa = (pgdir[index] & PHYSMASK);
    *out_addr = paging_pa2kva(pa);
    return 0;
}

int paging_walk_pt(pte_t *pt, void *va, void **out_addr)
{
    if (pt == NULL || va == NULL) {
        return -EINVAL;
    }

    int index = PGINDEX(va, 0);
    uintptr_t pa = (pt[index] & PHYSMASK);
    *out_addr = paging_pa2kva(pa);
    return 0;
}