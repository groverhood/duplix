#ifndef _RSDP_H_
#define _RSDP_H_

#include <stdint.h>
#include <static_assert.h>

struct acpiv1_rsdp {
    char r_sig[8];
    uint8_t r_checksum;
    char r_oemid[6];
    uint8_t r_revision;
    uint32_t r_physaddr;
};

struct acpiv2_rsdp {
    char r_sig[8];
    uint8_t r_chksum;
    char r_oemid[6];
    uint8_t r_revision;
    uint32_t r_rsdtpaddr;
    uint32_t r_length;
    uint64_t r_xsdtpaddr;
    uint8_t r_extchksum;
};

struct rsdt {
    char r_sig[4];
    uint32_t r_length;
    uint8_t r_revision;
    uint8_t r_chksum;
    char r_oemid[6];
    uint64_t r_oemtableid;
    uint32_t r_oemrevision;
    uint32_t r_creatorid;
    uint32_t r_paddrs[0];
};

struct xsdt {
    char x_sig[4];
    uint32_t x_length;
    uint8_t x_revision;
    uint8_t x_chksum;
    char x_oemid[6];
    uint64_t x_oemtableid;
    uint32_t x_oemrevision;
    uint32_t x_creatorid;
    uint64_t x_paddrs[0];
};

#endif