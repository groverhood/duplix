#include <reboot.h>
#include <stdio.h>


void reboot(int cmd)
{
    while (1) {
        asm volatile ("cli; hlt");
    }
}

int reboot_global(int cmd)
{
    reboot(cmd);
    return 0;
}