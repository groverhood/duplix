#ifndef _PAGING_H_
#define _PAGING_H_

/* See the diagram in pmap.h */
#include <pmap.h>
#include <stdint.h>
#include <stddef.h>

#define KERNEL_BASE 0xffff800000000000

extern size_t phys_ram_size;

typedef size_t pml4e_t;
typedef size_t pdpte_t;
typedef size_t pgdire_t;
typedef size_t pte_t;


int paging_uva2kva(pml4e_t *pml4, void *uva, void **kva);

static inline void *paging_pa2kva(uintptr_t pa)
{
    return (void *)(pa + KBASE);
}

static inline uintptr_t paging_kva2pa(void *kva)
{
    return ((uintptr_t)kva - KBASE);   
}

static inline pml4e_t *paging_pml4(void)
{
    uintptr_t phys_pml4;
    asm ("movq %%cr3, %[phys_pml4]" : [phys_pml4] "=r" (phys_pml4));
    return (pml4e_t *)paging_pa2kva(phys_pml4 & PHYSMASK);
}

int paging_map_pdpt2pml4(pml4e_t *pml4, int index, pdpte_t *pdpt, int perm);
int paging_map_pgdir2pdpt(pdpte_t *pdpt, int index, pgdire_t *pgdir, int perm);
int paging_map_page2pdpt(pdpte_t *pdpt, int index, void *gigapage, int perm);
int paging_map_pt2pgdir(pgdire_t *pagedir, int index, pte_t *pt, int perm);
int paging_map_page2pgdir(pgdire_t *pagedir, int index, void *hugepage, int perm);
int paging_map_page2pt(pte_t *pt, int index, void *page, int perm);

int paging_walk_pml4(pml4e_t *pml4, void *va, void **out_addr);
int paging_walk_pdpt(pdpte_t *pdpt, void *va, void **out_addr);
int paging_walk_pgdir(pgdire_t *pgdir, void *va, void **out_addr);
int paging_walk_pt(pte_t *pt, void *va, void **out_addr);

struct trapframe;

void page_fault(struct trapframe *tf);

#endif