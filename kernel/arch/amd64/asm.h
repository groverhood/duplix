#ifndef _ASM_H_
#define _ASM_H_

#include "gdt.h"
#include "interrupt.h"

#include <stddef.h>
#include <stdint.h>
#include <static_assert.h>

static inline void cpuid(int info, uint32_t *out_eax, uint32_t *out_ebx,
                         uint32_t *out_ecx, uint32_t *out_edx)
{
    asm volatile ("cpuid" 
                  : "=a" (*out_eax),
                    "=b" (*out_ebx),
                    "=c" (*out_ecx),
                    "=d" (*out_edx)
                  : "a" (info));
}

#define declcr(name) \
    static inline uint64_t ld##name(void) \
    { \
        uint64_t name; \
        asm volatile ("movq %%" #name ", %[" #name "]" : [name] "=r" (name));\
        return name; \
    } \
    static inline void st##name(uint64_t name##val) \
    { \
        asm volatile ("movq %[" #name "val], %%" #name :: [name##val] "r" (name##val)); \
    } \

declcr(cr0);
declcr(cr3);
declcr(cr4);

struct ljmp {
    uint64_t lj_addr;
    uint64_t lj_cs;
} __attribute__((packed, aligned (16)));

static inline void ljmp(volatile struct ljmp *addr)
{
    asm volatile (".byte 0x48\n\tljmp *%[addr]" :: [addr] "m" (*addr));
}

static inline void lgdt(struct gdtr *gdtr)
{
    asm volatile ("lgdt %0" :: "m" (*gdtr));
}

static inline void lidt(struct idtr *idtr)
{
    asm volatile ("lidt %0" :: "m" (*idtr));
}

static inline void lldt(uint16_t sel)
{
    asm volatile ("lldt %0" :: "r" (sel));
}

static inline uint8_t inb(uint16_t dx)
{
    uint8_t res;
    asm volatile ("inb %%dx, %0" : "=a" (res) : "d" (dx));
    return res;       
}

static inline void outb(uint8_t b, uint16_t dx)
{
    asm volatile ("outb %0, %%dx" :: "a" (b), "d" (dx));
}

static inline void cli(void)
{
    asm volatile ("cli");
}

static inline void sti(void)
{
    asm volatile ("sti");
}

static inline uint64_t rdmsr(uint32_t msr)
{
    uint32_t eax, edx;
    asm volatile ("rdmsr" : "=d" (edx), "=a" (eax) : "c" (msr));
    return ((uint64_t)edx << 32) | ((uint64_t)eax);
}

static inline void wrmsr(uint32_t msr, uint64_t value)
{
    asm volatile ("wrmsr" 
                :
                : "c" (msr), 
                  "d" (value >> 32), 
                  "a" (value & UINT32_MAX));
}

#endif