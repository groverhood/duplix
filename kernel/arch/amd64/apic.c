#include <fence.h>

#include "asm.h"
#include "msr.h"
#include "apic.h"
#include "kernio.h"

static iodesc_t apic_desc;
static volatile uint32_t *apic_base;

int apic_init(void)
{
    apic_desc = allocate_io();
    if (apic_desc < 0) {
        return (int)apic_desc;
    }
    wrmsr(IA32_APIC_BASE, (uint64_t)iodesc2kva(apic_desc));
    apic_base = (uint32_t *)rdmsr(IA32_APIC_BASE);
    return 0;
}

void apic_setbase(void *base)
{
    wrmsr(IA32_APIC_BASE, (uint64_t)base);
}

uint32_t apic_read32(int field)
{
    fence;
    /* == field * 16 */
    return apic_base[field << 2];
}

void apic_write32(int field, uint32_t value)
{
    fence;
    /* == field * 16 */
    apic_base[field << 2] = value;
}