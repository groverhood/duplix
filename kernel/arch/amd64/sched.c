#include <env.h>
#include <proc.h>
#include <stdio.h>

#include "../../sched.h"
#include "../../kenv.h"
#include "interrupt.h"
#include "apic.h"
#include "asm.h"

static int64_t ticks = 0;
int64_t kepoch;

static int timer_init(void);

int sched_init(void)
{
    int r;

    r = kenv_init();
    if (r < 0) {
        return r;
    }

    r = timer_init();
    if (r < 0) {
        return r;
    }

    return 0;
}

void handle_timer_interrupt(struct trapframe *tf)
{
    /**
     *  TODO: check if the allocated time period has passed
     **/
    cli();
    if (ticks++ >= kepoch) {
        kenv_yield();
    }
}

static int timer_init(void)
{
    int r;
    r = set_handler(TIMER_INTERRUPT, &handle_timer_interrupt);
    if (r < 0) {
        return r;
    }
    /* TODO: compute kepoch and add calls to apic interface */

    return 0;
}