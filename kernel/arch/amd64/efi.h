#ifndef _EFI_H_
#define _EFI_H_

#include <mmap.h>
#include <stdint.h>

struct efi_table_header {
    uint64_t hdr_signature;
    uint32_t hdr_revision;
    uint32_t hdr_size;
    uint32_t hdr_crc32;
    uint32_t _pad;
};

#define efiapi __attribute__((ms_abi))

typedef efiapi long (*efi_exit_boot_services)(void *efihandle, size_t mapkey);
typedef efiapi long (*efi_allocate_pool)(int memtype, size_t size, void **buffer);
typedef efiapi long (*efi_get_memorymap)();

struct efi_boot_services {
  ///
  /// The table header for the EFI Boot Services Table.
  ///
  struct efi_table_header                Hdr;
  //
  // Task Priority Services
  //
  void *                   RaiseTPL;
  void *                 RestoreTPL;

  //
  // Memory Services
  //
  void *              AllocatePages;
  void *                  FreePages;
  efi_get_memorymap              efi_get_memorymap;
  efi_allocate_pool               efi_allocate_pool;
  void *                   FreePool;

  //
  // Event & Timer Services
  //
  void *                  CreateEvent;
  void *                     SetTimer;
  void *                WaitForEvent;
  void *                  SignalEvent;
  void *                   CloseEvent;
  void *                   CheckEvent;

  //
  // Protocol Handler Services
  //
  void *    InstallProtocolInterface;
  void *  ReinstallProtocolInterface;
  void *  UninstallProtocolInterface;
  void *               HandleProtocol;
  void                              *Reserved;
  void *      RegisterProtocolNotify;
  void *                 LocateHandle;
  void *            LocateDevicePath;
  void *   InstallConfigurationTable;

  //
  // Image Services
  //
  void *                    LoadImage;
  void *                   StartImage;
  void *                          Exit;
  void *                  UnloadImage;
  efi_exit_boot_services            efi_exit_boot_services;

  //
  // Miscellaneous Services
  //
  void *      GetNextMonotonicCount;
  void *                         Stall;
  void *            SetWatchdogTimer;

  //
  // DriverSupport Services
  //
  void *            ConnectController;
  void *         DisconnectController;

  //
  // Open and Close Protocol Services
  //
  void *                 OpenProtocol;
  void *                CloseProtocol;
  void *     OpenProtocolInformation;

  //
  // Library Services
  //
  void *          ProtocolsPerHandle;
  void *          LocateHandleBuffer;
  void *               LocateProtocol;
  void *    InstallMultipleProtocolInterfaces;
  void *  UninstallMultipleProtocolInterfaces;

  //
  // 32-bit CRC Services
  //
  void *               CalculateCrc32;

  //
  // Miscellaneous Services
  //
  void *                      CopyMem;
  void *                       SetMem;
  void *               CreateEventEx;
};

///
/// EFI System Table
///
struct efi_system_table {
  ///
  /// The table header for the EFI System Table.
  ///
  struct efi_table_header                  Hdr;
  ///
  /// A pointer to a null terminated string that identifies the vendor
  /// that produces the system firmware for the platform.
  ///
  int16_t                            *FirmwareVendor;
  ///
  /// A firmware vendor specific value that identifies the revision
  /// of the system firmware for the platform.
  ///
  uint32_t                            FirmwareRevision;
  ///
  /// The handle for the active console input device. This handle must support
  /// void * and void *.
  ///
  void *                        ConsoleInHandle;
  ///
  /// A pointer to the void * interface that is
  /// associated with ConsoleInHandle.
  ///
  void    *ConIn;
  ///
  /// The handle for the active console output device.
  ///
  void *                        ConsoleOutHandle;
  ///
  /// A pointer to the void * interface
  /// that is associated with ConsoleOutHandle.
  ///
  void   *ConOut;
  ///
  /// The handle for the active standard error console device.
  /// This handle must support the void *.
  ///
  void *                        StandardErrorHandle;
  ///
  /// A pointer to the void * interface
  /// that is associated with StandardErrorHandle.
  ///
  void   *StdErr;
  ///
  /// A pointer to the EFI Runtime Services Table.
  ///
  void              *RuntimeServices;
  ///
  /// A pointer to the EFI Boot Services Table.
  ///
  struct efi_boot_services                 *st_bootservices;
  ///
  /// The number of system configuration tables in the buffer ConfigurationTable.
  ///
  size_t                             NumberOfTableEntries;
  ///
  /// A pointer to the system configuration tables.
  /// The number of entries in the table is NumberOfTableEntries.
  ///
  void           *ConfigurationTable;
};

long exitefi(void *handle, struct efi_boot_services *bs, 
            struct memorymap **map, unsigned long *descsize, unsigned long *size) __attribute__((section (".boot.text")));

#endif