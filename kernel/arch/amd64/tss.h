#ifndef _TSS_H_
#define _TSS_H_

#include <stdint.h>

struct tss {
    uint32_t _pad0;
    uint32_t t_rsp0_lower;
    uint32_t t_rsp0_upper;
    uint32_t t_rsp1_lower;
    uint32_t t_rsp1_upper;
    uint32_t t_rsp2_lower;
    uint32_t t_rsp2_upper;
    uint32_t _pad1;
    uint32_t _pad2;
    uint32_t t_ist1_lower;
    uint32_t t_ist1_upper;
    uint32_t t_ist2_lower;
    uint32_t t_ist2_upper;
    uint32_t t_ist3_lower;
    uint32_t t_ist3_upper;
    uint32_t t_ist4_lower;
    uint32_t t_ist4_upper;
    uint32_t t_ist5_lower;
    uint32_t t_ist5_upper;
    uint32_t t_ist6_lower;
    uint32_t t_ist6_upper;
    uint32_t t_ist7_lower;
    uint32_t t_ist7_upper;
    uint32_t _pad3;
    uint32_t _pad4;
};

#endif