#include <ctype.h>

#include "panic.h"
#include "kernio.h"
#include "kserial.h"
#include "multiboot.h"

#include "../../pgalloc.h"
#include "../../sched.h"

struct memmap_info {
    struct memorymap *mmi_mmap;
    size_t mmi_size;
    unsigned mmi_descsize;
};

static int parse_kargs(struct multiboot2_command_line *cmdline, int *argc, char **argv);

void _start(void)
{
    int argc;
    long efierror;
    static char *argv[32];
    struct memmap_info *mmi;
    struct multiboot2_info_tag *info;
    struct multiboot2_info_tag *rsdp;
    struct multiboot2_bootinfo *bootinfo;
    struct multiboot2_command_line *cmdline;
    asm volatile ("movq %%rax, %[bootinfo]\n\t"
                  "movq %%rbx, %[mmi]\n\t" 
                  "movq %%rcx, %[efierror]"
                  : [bootinfo] "=r" (bootinfo),
                    [mmi] "=r" (mmi),
                    [efierror] "=r" (efierror)
                  :: "rax", "rbx", "rcx");

    if (serial_init() < 0) {
        reboot(REBOOT_NORETURN);
    }
    
    if (efierror < 0) {
        panic("EFI Error: %#016lx", (unsigned long)efierror);
        reboot(REBOOT_NORETURN);
    }

    for (info = mb_bootinfo_begin(bootinfo); info != mb_bootinfo_end(bootinfo); info = mb_bootinfo_next(info)) {
        switch (info->in_type) {
            case MBINFO_CMDLINE:
                cmdline = (struct multiboot2_command_line *)info;
            break;
            case MBINFO_NEWRSDP:
                rsdp = info;
            break;
        }
    }

    printf("Initializing page allocator... ");
    if (pgalloc_init(mmi->mmi_mmap, mmi->mmi_size, mmi->mmi_descsize) < 0) {
        panic("\nFailed to initialize page allocator");
    }

    puts("DONE"); 
    print_freelist();

    printf("Initializing I/O... ");
    if (init_io(rsdp) < 0) {
        panic("Failed initializing I/O");
    }

    printf("DONE\nInitializing scheduler... ");
    if (sched_init() < 0) {
        panic("Failed initializing kernel env system");
    }

    printf("DONE\nParsing kernel args... ");
    if (parse_kargs(cmdline, &argc, argv) < 0) {
        panic("Error parsing kernel args");
    }

    puts("DONE\n\nFinished initializing kernel!");
    if (main(argc, argv) != 0) {
        panic("main() returned non-zero exit status");
    } else {
        reboot(REBOOT_NORETURN);
    }
}

static int parse_kargs(struct multiboot2_command_line *cmdline, int *argc, char **argv)
{
    int r;
    int argcnt = 1;    
    size_t off = 0;
    size_t len = mb_cmdline_strlen(cmdline);
    char *str = mb_cmdline_string(cmdline);
    argv[0] = process;
    while (off < len) {
        while (isspace(str[off]) && off < len) {
            off++;
        }    
        if (off >= len) {
            break;
        }
        argv[argcnt++] = &str[off];
        while (!isspace(str[off]) && off < len) {
            off++;
        }
    }
    *argc = argcnt;
    return (r < 0) ? r : argcnt;
}