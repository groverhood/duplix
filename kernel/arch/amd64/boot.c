
#include <mmap.h>
#include <ctype.h>
#include <stdio.h>
#include <stdint.h>

#include "efi.h"
#include "asm.h"
#include "gdt.h"
#include "tss.h"
#include "paging.h"
#include "interrupt.h"
#include "multiboot.h"

static int init_gdt(void) __attribute__((section (".boot.text")));
static inline int paging_init(void) __attribute__((section (".boot.text")));

#define spin() do { asm volatile ("cli\n\t1:\n\nhlt\n\tjmp 1b"); } while (0)

static volatile pml4e_t boot_pml4[PG_ENTRIES] __attribute__((section (".boot.bss"), aligned (PGSIZE)));
static volatile pdpte_t boot_pdpt[PG_ENTRIES] __attribute__((section (".boot.bss"), aligned (PGSIZE)));

#define NGIGS 64

static struct multiboot2_bootinfo *bootinfo __attribute__((section (".boot.bss")));

static inline int paging_init(void)
{
    long r;
    size_t i;
    size_t ofs;
    int flags;
    uintptr_t va;
    flags = PTE_PS | PTE_RDWR | PTE_P;
    for (i = 0; i < NGIGS; ++i) {
        ofs = i * GIGASIZE;
        boot_pdpt[PGINDEX(ofs, 2)] = PGENTRY(ofs, flags);
    }
    boot_pml4[PGINDEX(0, 3)] = PGENTRY(boot_pdpt, PTE_RDWR | PTE_P);
    boot_pml4[PGINDEX(KERNEL_BASE, 3)] = PGENTRY(boot_pdpt, PTE_RDWR | PTE_P);
    stcr3((uint64_t)boot_pml4);
    r = init_gdt();
    if (r < 0) {
        return r;
    }
    return 0;
}

static struct memmap_info {
    struct memorymap *mmi_mmap;
    unsigned long mmi_size;
    unsigned long mmi_descsize;    
} memmap_info __attribute__((section (".boot")));

static char kernstk[PGSIZE] __attribute__((section (".boot.bss"), aligned (PGSIZE)));

void _boot(void) __attribute__((section (".boot.text"))); 
void _boot(void)
{
    long r;
    int argc;
    uint64_t magic;
    uintptr_t bootinfo_phys;
    void *efihandle;
    unsigned size;
    unsigned descsize;
    struct memorymap *efimmap;
    struct efi_boot_services *bs;
    struct efi_system_table *systab;
    struct multiboot2_info_tag *info;
    struct memmap_info *pmmap_info;

    register void *volatile rsp asm("rsp");
    volatile struct ljmp jmpaddr;

    asm volatile ("movq %%rax, %[magic]\n\t"
                  "movq %%rbx, %[bootinfo]"
                : [magic] "=r" (magic),
                  [bootinfo] "=r" (bootinfo_phys) 
                :
                : "rax", "rbx");

    bootinfo = (struct multiboot2_bootinfo *)bootinfo_phys;
    for (info = mb_bootinfo_begin(bootinfo); info != mb_bootinfo_end(bootinfo); info = mb_bootinfo_next(info)) {
        if (info->in_type == MBINFO_ESYSTAB) {
            systab = ((struct multiboot2_systab *)info)->sys_pointer;
            bs = systab->st_bootservices;
            break;
        }
    }

    if ((r = exitefi(efihandle, bs, &memmap_info.mmi_mmap, &memmap_info.mmi_descsize, &memmap_info.mmi_size)) < 0) {
        bootinfo = NULL;
        pmmap_info = NULL;
    } else {
        memmap_info.mmi_mmap = paging_pa2kva((uintptr_t)memmap_info.mmi_mmap);
        pmmap_info = &memmap_info;    
    }
    
    if (paging_init() < 0) {
        bootinfo = NULL;
        pmmap_info = NULL;
    } else {
        bootinfo = paging_pa2kva((uintptr_t)bootinfo);
        pmmap_info = paging_pa2kva((uintptr_t)pmmap_info);
    }

    rsp = paging_pa2kva((uintptr_t)kernstk);
    jmpaddr.lj_cs = seg2ofs(GDT_KCODE);
    asm volatile ("movabs $_start, %[jmpaddr__lj_addr]" : [jmpaddr__lj_addr] "=r" (jmpaddr.lj_addr));
    asm volatile ("movq %[bootinfo], %%rax\n\t"
                  "movq %[memmap_info], %%rbx\n\t"
                  "movq %[error], %%rcx" 
                  :: [bootinfo] "r" (bootinfo),
                     [memmap_info] "r" (pmmap_info),
                     [error] "r" (r) : "rax", "rbx", "rcx");
    ljmp(&jmpaddr);
    spin();
}

static struct segdesc segdescs[] __attribute__((section (".boot.data"))) = {
    NULL_SEGDESC,
    [GDT_KCODE] = mksegdesc(0, true, SEGTYPE_CODE_EXECRD),
    [GDT_KDATA] = mksegdesc(0, true, SEGTYPE_DATA_RW),
    [GDT_UCODE] = mksegdesc(3, true, SEGTYPE_CODE_EXECRD),
    [GDT_UDATA] = mksegdesc(3, true, SEGTYPE_DATA_RW),
    // IDT
    NULL_SEGDESC,
    NULL_SEGDESC,
    // TSS
    NULL_SEGDESC,
    NULL_SEGDESC
};

static int init_gdt(void)
{
    struct gdtr gdtr = mkgdtr(segdescs); 
    lgdt(&gdtr);
    lldt(0);
    return 0;    
}