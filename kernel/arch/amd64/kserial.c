
#define PORT 0x3f8

#include "asm.h"
#include "serial.h"

static spinlock_t lock;

int serial_init(void)
{
    outb(0,    PORT + 1);
    outb(0x80, PORT + 3);
    outb(1,    PORT    );
    outb(0,    PORT + 1);
    outb(3,    PORT + 3);
    outb(0xc7, PORT + 2);
    outb(8,    PORT + 4);
    return 0;
}


int putchar(int c)
{
    while (inb(PORT + 5) & 0x20 == 0);
    outb(c, PORT);
    return c;    
}

int puts(const char *restrict s)
{
    while (*s) {
        putchar(*s++);
    }
    putchar('\n');
}

int putbuf(void *buf, size_t count)
{
    while (count-- > 0) {
        putchar(*(char *)buf++);
    }
}