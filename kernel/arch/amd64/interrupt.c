#include <pmap.h>
#include <stdint.h>
#include <stdbool.h>
#include <static_assert.h>

#include "apic.h"
#include "paging.h"
#include "interrupt.h"

struct idt_gate_descriptor {
    uint16_t idt_offset_low;
    uint16_t idt_ss;
    uint8_t idt_ist;
    uint8_t idt_attr;
    uint16_t idt_offset_mid;
    uint32_t idt_offset_high;
    uint32_t _pad;
} __attribute__((packed));

struct idt_gate_metadata {
    interrupt_handler_t ig_handler;
    int ig_vec;   
};

static_assert(sizeof(struct idt_gate_descriptor) == 16, "idt_gate_descriptor improperly packed");

static void idt_set_addr(struct idt_gate_descriptor *desc, void *kva);
static void idt_set_present(struct idt_gate_descriptor *desc, bool p);
static void idt_set_privilege(struct idt_gate_descriptor *desc, int level);
static void idt_set_type(struct idt_gate_descriptor *desc, int type);
static void idt_set_ss(struct idt_gate_descriptor *desc, uint16_t segsel);

static struct idt_gate_descriptor idt[INTERRUPT_COUNT] ;
static struct idt_gate_metadata fake_idt[INTERRUPT_COUNT];

static_assert(sizeof idt == PGSIZE, "idt doesn't fit within a page");

int init_interrupts(void)
{
    int r;
    int vec;
    for (vec = 0; vec < INTERRUPT_COUNT; ++vec) {
        idt_set_addr(&idt[vec], &generic_handler);
        idt_set_present(&idt[vec], true);
        idt_set_privilege(&idt[vec], 0);
        // TODO: change these
        idt_set_type(&idt[vec], 0);
        idt_set_ss(&idt[vec], 0);   
    }
    r = apic_init();
    if (r < 0) {
        return r;
    }
    return 0;
}

int set_handler(int vec, interrupt_handler_t handler)
{
    fake_idt[vec].ig_handler = handler; 
    return 0;
}

static void idt_set_addr(struct idt_gate_descriptor *desc, void *kva)
{
    uintptr_t pa = paging_kva2pa(kva);
    desc->idt_offset_low = (pa & 0xffff);
    desc->idt_offset_mid = ((pa >> 16) & 0xffff);
    desc->idt_offset_high = (pa >> 32);
}

static void idt_set_present(struct idt_gate_descriptor *desc, bool p)
{
    desc->idt_attr &= ~(1 << 7);
    desc->idt_attr |= ((int)p << 7);
}

static void idt_set_privilege(struct idt_gate_descriptor *desc, int level)
{
    desc->idt_attr &= ~(0x3 << 5);
    desc->idt_attr |= ((level & 0x3) << 5);
}

static void idt_set_type(struct idt_gate_descriptor *desc, int type)
{
    desc->idt_attr &= ~0x7;
    desc->idt_attr |= (type & 0x7);
}

static void idt_set_ss(struct idt_gate_descriptor *desc, uint16_t segsel)
{
    desc->idt_ss = segsel;
}

