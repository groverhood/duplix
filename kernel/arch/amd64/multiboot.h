#ifndef _MULTIBOOT_H_
#define _MULTIBOOT_H_

// TODO: add header flags

#define MB_MAGIC 0xe85250d6

#define MBARCH_I386 0
#define MBARCH_MIPS 4

#define MB_OPTIONAL 0
#define MB_REQUIRED 1

#define MBTAG_INFOREQ  1
#define MBTAG_ENTRY    3
#define MBTAG_CONSOLE  4
#define MBTAG_FRAMEBUF 5
#define MBTAG_MODALGN  6
#define MBTAG_EFIBOOT  7
#define MBTAG_EFIX64   9
#define MBTAG_RELOCHDR 10

#define MBCONS_REQONE (1 << 0)
#define MBCONS_EGA    (1 << 1)

#define MBINFO_CMDLINE  1
#define MBINFO_MODULES  3
#define MBINFO_MB2MMAP  6
#define MBINFO_FRAMBUF  8
#define MBINFO_ELFSYMS  9
#define MBINFO_ESYSTAB 12
#define MBINFO_OLDRSDP 14
#define MBINFO_NEWRSDP 15
#define MBINFO_NETINFO 16
#define MBINFO_EFIMMAP 17
#define MBINFO_IMAGPTR 20

#ifndef __ASM__

#include <mmap.h>
#include <stddef.h>
#include <stdint.h>

#include "../../acpi.h"
#include "paging.h"

struct multiboot2_bootinfo {
    uint32_t bi_total_size;
    uint32_t _pad;
};

struct multiboot2_info_tag {
    uint32_t in_type;
    uint32_t in_size;
};

static inline struct multiboot2_info_tag *mb_bootinfo_begin(struct multiboot2_bootinfo *bootinfo)
{
    return (struct multiboot2_info_tag *)(bootinfo + 1);
}

static inline struct multiboot2_info_tag *mb_bootinfo_end(struct multiboot2_bootinfo *bootinfo)
{
    return (struct multiboot2_info_tag *)((uint8_t *)bootinfo + bootinfo->bi_total_size);
}

static inline struct multiboot2_info_tag *mb_bootinfo_next(struct multiboot2_info_tag *info)
{
    return (struct multiboot2_info_tag *)((uint8_t *)info + 8 * ((info->in_size + 8 - 1) / 8));
}

struct multiboot2_command_line {
    struct multiboot2_info_tag cmd_header;
    uint8_t cmd_string[0];
};

static inline size_t mb_cmdline_strlen(struct multiboot2_command_line *cmdline)
{
    return cmdline->cmd_header.in_size - sizeof cmdline->cmd_header - 1; 
}

static inline char *mb_cmdline_string(struct multiboot2_command_line *cmdline)
{
    return (char *)cmdline->cmd_string;
}

struct multiboot2_systab {
    struct multiboot2_info_tag sys_header;
    void *sys_pointer;
};

struct multiboot2_image {
    struct multiboot2_info_tag img_header;
    void *img_handle;
};

struct multiboot2_module {
    struct multiboot2_info_tag mod_header;
    uint32_t mod_start;
    uint32_t mod_end;
    uint8_t mod_string[0];
};

static inline size_t mb_module_strlen(struct multiboot2_module *mod)
{
    return mod->mod_header.in_size - sizeof mod->mod_header;
}

static inline char *mb_module_string(struct multiboot2_module *mod)
{
    return (char *)mod->mod_string;
}

static inline void *mb_module_ptr(struct multiboot2_module *mod)
{
    return paging_pa2kva(mod->mod_start);
}

static inline size_t mb_module_length(struct multiboot2_module *mod)
{
    return (mod->mod_end - mod->mod_start);    
}

struct multiboot2_old_rsdp {
    struct multiboot2_info_tag rs_header;
    struct acpiv1_rsdp rs_rsdp;
};

struct multiboot2_new_rsdp {
    struct multiboot2_info_tag rs_header;
    struct acpiv2_rsdp rs_rsdp;
};

struct multiboot2_efi_memory_map {
    struct multiboot2_info_tag mm_header;
    uint32_t mm_descsize;
    uint32_t mm_descversion;
    struct memorymap mm_mmap[0];
};

static inline size_t mb_mmap_size(struct multiboot2_efi_memory_map *efimmap)
{
    return efimmap->mm_header.in_size - sizeof efimmap->mm_header;
}

#endif

#endif