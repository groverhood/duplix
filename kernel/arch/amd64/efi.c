#include <mmap.h>
#include <pmap.h>
#include <stdio.h>
#include <stddef.h>

#include "efi.h"
#include "paging.h"

enum {
    ALLOCATE_ANY_PAGES,
    ALLOCATE_MAX_ADDRESS,
    ALLOCATE_ADDRESS
};

long exitefi(void *handle, struct efi_boot_services *bs, 
            struct memorymap **map, unsigned long *descsize,
            unsigned long *size)
{
    long r;
    uint64_t mapkey;
    *size = 0;
    r = bs->efi_get_memorymap(size, NULL, NULL, NULL, NULL);
    r = bs->efi_allocate_pool(MM_TYPE_LOADERDATA, *size, (void **)map);
    if (r < 0) {
        return r;
    }
    //*size = PGSIZE * ((*size + PGSIZE - 1) / PGSIZE);
    r = bs->efi_get_memorymap(size, *map, &mapkey, descsize, NULL);
    if (r < 0) {
        return r;
    }
    bs->efi_exit_boot_services(handle, mapkey);
    return 0;
}