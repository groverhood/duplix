
#include <syscn.h>
#include <errno.h>
#include <trapframe.h>

#include "paging.h"
#include "../../kenv.h"
#include "../../ksyscall.h"

typedef int (*syscall_handler_t)(struct trapframe *tf);

/*  */
static int sys_ipc_send(struct trapframe *tf);

static syscall_handler_t syscalls[SYSCALL_COUNT] = {
};

int init_syscalls(void)
{
    /* TODO: write to LSTAR and STAR msrs */
    return 0;
}

void handle_syscall(struct trapframe *tf)
{
    void *restore;
    syscnum_t callnum;
    register void *volatile rsp asm("rsp");
    restore = rsp;
    rsp = ((struct kenv *)curenv)->ke_kernstk;
    callnum = tf->tf_regs.rg_rax;
    if (callnum > -1 && callnum < SYSCALL_COUNT) {
        tf->tf_regs.rg_rax = syscalls[callnum](tf);
    } else {
        tf->tf_regs.rg_rax = -EINVAL;
    }
    rsp = restore;
    asm volatile ("sysret" :: "c" (tf->tf_rip));
    __builtin_unreachable();
}

static int sys_ipc_send(struct trapframe *tf)
{
    int r;

    return 0;
}