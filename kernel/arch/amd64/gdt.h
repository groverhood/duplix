#ifndef _GDT_H_
#define _GDT_H_

#include <stdint.h>
#include <static_assert.h>

struct gdtr {
    uint16_t gr_limit;
    uint64_t gr_baseaddr;
} __attribute__((packed));

struct segdesc {
    uint16_t sd_limit_low;
    uint32_t sd_baseaddr_low : 24;
    uint8_t sd_type : 4;
    uint8_t sd_codedata : 1;
    uint8_t sd_privlevel : 2;
    uint8_t sd_present : 1;
    uint8_t sd_limit_high : 4;
    uint8_t sd_avail : 1;
    uint8_t sd_64bit : 1;
    uint8_t sd_defaultopsz : 1;
    uint8_t sd_granularity : 1;
    uint8_t sd_baseaddr_high;
} __attribute__((packed));

enum {
    SEGTYPE_DATA_RDONLY,
    SEGTYPE_DATA_RDONLYA,
    SEGTYPE_DATA_RW,
    SEGTYPE_DATA_RWA,
    SEGTYPE_DATA_RDONLYD,
    SEGTYPE_DATA_RDONLYDA,
    SEGTYPE_DATA_RWD,
    SEGTYPE_DATA_RWDA,
    SEGTYPE_CODE_EXECONLY,
    SEGTYPE_CODE_EXECONLYA,
    SEGTYPE_CODE_EXECRD,
    SEGTYPE_CODE_EXECRDA,
    SEGTYPE_CODE_EXECONLYC,
    SEGTYPE_CODE_EXECONLYCA,
    SEGTYPE_CODE_EXECRDC,
    SEGTYPE_CODE_EXECRDCA
};

enum {
    GDT_KCODE = 1,
    GDT_KDATA,
    GDT_UCODE,
    GDT_UDATA,
    GDT_IDT,
    GDT_TSS = GDT_IDT + 2
};

static inline int seg2ofs(uint16_t seg)
{
    return (seg << 3);
}

#define gdtofs(e) ((e) << 3)
#define NULL_SEGDESC (struct segdesc){ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

static_assert(sizeof(struct segdesc) == 8, "call_gate is improperly padded");

#define mksegdesc(privlevel, codedata, type) \
    (struct segdesc){ .sd_present = 1, \
                      .sd_privlevel = privlevel, \
                      .sd_64bit = 1, \
                      .sd_present = 1, \
                      .sd_codedata = codedata, \
                      .sd_type = type }
#define mkgdtr(segdescs) \
    (struct gdtr){ .gr_limit = (sizeof (segdescs) - 1), \
                   .gr_baseaddr = (uint64_t)(segdescs) } 

#endif