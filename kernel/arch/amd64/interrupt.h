#ifndef _INTERRUPT_H_
#define _INTERRUPT_H_

/**
 *  Read page 188 of Intel Software Developer System Programming Manual Vol 3
 **/

struct trapframe;

typedef void (*interrupt_handler_t)(struct trapframe *tf);

int init_interrupts(void);

int set_handler(int vec, interrupt_handler_t handler);

void generic_handler();

struct idtr {
    uint16_t i_limit;
    uint64_t i_baseaddr;    
} __attribute__((packed));

enum {
    DIVIDE_ERROR,
    DEBUG_EXCEPTION,
    NMI_INTERRUPT,
    BREAKPOINT,
    OVERFLOW,
    BOUND,
    INVALID_OPCODE,
    NO_MATH_COPROCESSOR,
    DOUBLE_FAULT,
    COPROCESSOR_SEGMENT_OVERRUN,
    INVALID_TSS,
    SEGMENT_NOT_PRESENT,
    STACK_SEGMENT_FAULT,
    GENERAL_PROTECTION,
    PAGE_FAULT,
    MATH_FAULT = 16,
    ALIGNMENT_CHECK,
    MACHINE_CHECK,
    SIMD_EXCEPTION,
    EPT_VIOLATION,
    USER_MIN = 32,
    TIMER_INTERRUPT = USER_MIN,
    INTERRUPT_COUNT = 256
};

#endif 