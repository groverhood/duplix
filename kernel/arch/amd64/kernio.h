#ifndef _KERNIO_H_
#define _KERNIO_H_

#include <fs.h>

#include "multiboot.h"

enum { KERNIOBASE = 0xffffffff80000000,
       KERNIOSIZE = 0x70000000 };

/* I/O descs are kernel level capabilities used so that there is
   no discrimination between how a pre-upper half kernel operates
   and how an upper half kernel operates. */
typedef off_t iodesc_t;

/* Check this to make sure whether or not kernel I/O is fully
   initialized. */
extern int io_init;

int init_io(struct multiboot2_info_tag *rsdp);
iodesc_t allocate_io(void);
void *iodesc2kva(iodesc_t desc);

#endif