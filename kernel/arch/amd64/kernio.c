#include "../../acpi.h"
#include "kernio.h"
#include "paging.h"

static off_t iobumpptr = 0;
static uint8_t *iobase = (uint8_t *)KERNIOBASE;

int init_io(struct multiboot2_info_tag *rsdp)
{
    int r;
    uint32_t type = rsdp->in_type;
    if (type == MBINFO_NEWRSDP) {
        r = init_acpiv2(&((struct multiboot2_new_rsdp *)rsdp)->rs_rsdp);
    } else if (type == MBINFO_OLDRSDP) {
        r = init_acpiv1(&((struct multiboot2_old_rsdp *)rsdp)->rs_rsdp);
    } else {
        r = -EINVAL;
    }

    /**
     * TODO: 
     *  Initialize I/O descriptor allocation and I/O descriptor table
     *  Traverse the PCI/PCIe configuration space
     *  Memory map devices as they're found within the kernel MMIO space
     **/

    return (r < 0) ? r : 0;
}

iodesc_t allocate_io(void)
{
    iodesc_t desc = iobumpptr;
    iobumpptr += PGSIZE;
    return desc;
}

void *iodesc2kva(iodesc_t desc)
{
    return (iobase + desc);
}

