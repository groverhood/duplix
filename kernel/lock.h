#ifndef _LOCK_H_
#define _LOCK_H_

#include <list.h>
#include <proc.h>
#include <spinlock.h>

struct lock {
    spinlock_t l_guard;
    int l_held;
    struct list l_queue;
};

void lock_init(struct lock *lock);

void lock_acquire(struct lock *lock);
void lock_release(struct lock *lock);

#endif