
#include <panic.h>
#include <reboot.h>

char *process = "/bin/kernel";

static int shutdown_when_finished;

static void parse_args(int argc, char *argv);

int main(int argc, char *argv[])
{
    printf("%i\n", argc);

    if (shutdown_when_finished) {
        reboot(REBOOT_NORETURN);
    }
    return 0;
}

static void parse_args(int argc, char *argv)
{

}