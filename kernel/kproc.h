#ifndef _KPROC_H_
#define _KPROC_H_

#include <pmap.h>
#include <proc.h>

struct kproc {
    struct proc kp_proc;
    uint8_t _pad[PGSIZE - sizeof(struct proc)];

    void *kp_ptroot;
    void *kp_eptroot;
};

#define KPROCPAGES ((sizeof(struct kproc *) + PGSIZE - 1) / PGSIZE) 
#define KPROCSIZE (PGSIZE * KPROCPAGES)

static_assert(KPROCSIZE % PGSIZE == 0, "kproc not page aligned");
static_assert(offsetof(struct kproc, kp_ptroot) == PGSIZE, "kproc improperly padded");

#endif