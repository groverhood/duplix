#ifndef _EVENT_H_
#define _EVENT_H_

#include <env.h>
#include <proc.h>
#include <stdint.h>
#include <syscall.h>
#include <trapframe.h>

typedef int32_t evttype_t;

enum {
    EVT_UNIX,       // >>> UNIX signal
    EVT_IPC_INIT,   // >>> IPC init
    EVT_IPC_SIGNAL, // >>> IPC wakeup
    EVT_MTXUNLCK,   // >>> Wake up thread
    EVT_DEVMAP,
    EVT_TYPE_COUNT,
};

#define EVT_RETURN (1 << 31)

struct thread_sem;

struct event_packet {
    envid_t evt_sendenv;
    procid_t evt_sendproc;
    evttype_t evt_type;
    int *evt_status;
    int evt_res;
    struct thread_sem *evt_sem;
};

int init_events(void);

/* Add a trigger the process to schedule a thread using its event handler */
static inline int sendevt(envid_t env, procid_t proc, struct event_packet *payload)
{
    return syscall3(SYS_SEND_EVENT, env, proc, (uint64_t)payload);
}

typedef int (*evthandler_t)(struct event_packet *pkt);

int evthandler(evttype_t evt, evthandler_t handler);

void handle_event(struct trapframe *tf, struct event_packet *pkt);

#define PACKET_HEADER struct event_packet evt_pkt
#define PACKET_INIT(typ, pstatus, psem) .evt_pkt = { .evt_type = typ, \
                                                     .evt_status = pstatus, \
                                                     .evt_sem = psem, \
                                                     .evt_sendenv = curproc->pr_env->e_self, \
                                                     .evt_sendproc = curproc->pr_self } 

#endif