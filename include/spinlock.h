#ifndef _SPINLOCK_H_
#define _SPINLOCK_H_

#include <stdatomic.h>

typedef atomic_bool spinlock_t;

static inline void spinlock_init(spinlock_t *lock)
{
    atomic_init(lock, false);
}

static inline void spinlock_acquire(spinlock_t *lock)
{
    while (atomic_test_and_set(lock) == true) asm ("");
}

static inline void spinlock_release(spinlock_t *lock)
{
    atomic_store(lock, 0);    
}

#endif