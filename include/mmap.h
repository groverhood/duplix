#ifndef _MMAP_H_
#define _MMAP_H_

#include <stdint.h>
#include <stddef.h>

/* EFI Memory Map */
struct memorymap {
    uint32_t mm_type;
    uintptr_t mm_phys;
    uintptr_t mm_virt;
    uint64_t mm_npages;
    uint64_t mm_attr;
};

#define MM_ATTR_UC            0x0000000000000001
#define MM_ATTR_WC            0x0000000000000002
#define MM_ATTR_WT            0x0000000000000004
#define MM_ATTR_WB            0x0000000000000008
#define MM_ATTR_UCE           0x0000000000000010
#define MM_ATTR_WP            0x0000000000001000
#define MM_ATTR_RP            0x0000000000002000
#define MM_ATTR_XP            0x0000000000004000
#define MM_ATTR_NV            0x0000000000008000
#define MM_ATTR_MORE_RELIABLE 0x0000000000010000
#define MM_ATTR_RO            0x0000000000020000
#define MM_ATTR_SP            0x0000000000040000
#define MM_ATTR_CPU_CRYPTO    0x0000000000080000
#define MM_ATTR_RUNTIME       0x8000000000000000

enum {
    MM_TYPE_RESERVED,
    MM_TYPE_LOADERCODE,
    MM_TYPE_LOADERDATA,
    MM_TYPE_BOOTSRVCODE,
    MM_TYPE_BOOTSRVDATA,
    MM_TYPE_RTSRVCODE,
    MM_TYPE_RTSRVDATA,
    MM_TYPE_CONVENTIONAL,
    MM_TYPE_UNUSABLE,
    MM_TYPE_ACPIRECL,
    MM_TYPE_ACPINVS,
    MM_TYPE_MMAPIO,
    MM_TYPE_MMAPIOPS,
    MM_TYPE_PALCODE,
    MM_TYPE_PERSISTENT,
    MM_TYPE_MAX
};

static inline struct memorymap *mm_next(struct memorymap *mmap, size_t mmsize)
{
    return (struct memorymap *)((uint8_t *)mmap + mmsize);
}

#endif