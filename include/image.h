#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>
#include <static_assert.h>

struct imagehdr {
    uint32_t im_version; // >>> Duplix Image version
    uint32_t im_size;    // >>> Size of the header struct
    uint64_t im_unsize;  // >>> Total size of the unit section
    uint64_t im_fssize;  // >>> Total size of the filesystem section
} __attribute__((packed));

struct imageunit {
    uint32_t un_size;
    uint32_t un_type;
    uint64_t un_blkoff;
    uint64_t un_blksize;
} __attribute__((packed));

enum /* unit_type */ {
    UNIT_USER,
    UNIT_INIT,
    UNIT_FILESERVER,
    UNIT_NETSERVER,
};

/**
 *  =====
 *  / -- NULL, true (abs)
 *     | /
 *     | /bin
 *     | /etc
 *     | /root
 *     | /var
 *     | /dev
 *     | /lib
 *  =====
 *  /bin/python -- /, true
 *     | /bin/python (file)
 *     | /lib/python (dir)
 **/

struct imagefsnode {
    uint64_t fs_size;       // >>> Size of the node in blocks
    uint64_t fs_inodebase;  // >>> Base block address for the inode
    uint64_t fs_inodeid;    // >>> Inode UID
    uint64_t fs_attr;       // >>> See union imagefsattr
    char name[32];          // >>> Identifier used for keying
} __attribute__((packed));

union un_imagefsattr {
    struct imagefsattr {
        uint8_t fs_abs : 1;
    } __attribute__((packed)) un_attr;    
    uint64_t un_raw;
};

static_assert(sizeof(union un_imagefsattr) == sizeof(uint64_t), "imagefsattr improperly packed");

static inline struct imagefsattr imgnode_raw2attr(uint64_t raw)
{
    return ((union un_imagefsattr){ .un_raw = raw }).un_attr;
}

static inline struct imageunit *imunit_start(struct imagehdr *hdr)
{
    return (struct imageunit *)((uint8_t *)hdr + hdr->im_size);
}

static inline struct imageunit *imunit_end(struct imagehdr *hdr)
{
    return (struct imageunit *)((uint8_t *)hdr + hdr->im_unsize);
}

static inline struct imageunit *imunit_next(struct imageunit *unit)
{
    return (struct imageunit *)((uint8_t *)unit + unit->un_size);
}

int image(const char *imgfile, struct imagehdr **out_im);
int image2(uint8_t *img, struct imagehdr **out_im);
void freeimg(struct imagehdr *im);

#endif