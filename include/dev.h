#ifndef _DEV_H_
#define _DEV_H_

#include <crt.h>
#include <event.h>

typedef int dev_t;

typedef int devtype_t;

enum { 
    DEV_SATA
};

struct devstat {
    dev_t st_handle;
    devtype_t st_type;

};

struct thread_shared_mutex;
struct thread_sem;

struct devmap_packet {
    PACKET_HEADER;
    void *dvm_devva;
    void *dvm_mtxva;
    dev_t dvm_dev;
};

struct dev {
    dev_t dev_handle;
    devtype_t dev_type;
    struct thread_shared_mutex *dev_mtx;
    void *dev_va;
};

weaksym ctor int dev_init(void);
int devstat(devtype_t type, size_t *devcount, struct devstat statbuf[]);
int devmap(dev_t dev, struct dev *devobj);

#endif