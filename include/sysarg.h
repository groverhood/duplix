#ifndef _SYSARG_H_
#define _SYSARG_H_

#include <stdint.h>

typedef uint64_t sysarg_t;

struct sysargs {
    sysarg_t args[6];
};

#endif