#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <event.h>

struct serial_packet {
    PACKET_HEADER;
    int se_endpoint;
    size_t se_bytes;
    void *se_buffer;
};

typedef int serial_putc_t(int c);

extern serial_putc_t putchar;

int puts(const char *__restrict__ s);

int putbuf(void *buf, size_t count);

#endif