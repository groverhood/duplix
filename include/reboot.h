#ifndef _SHUTDOWN_H_
#define _SHUTDOWN_H_

/**
 * Restart the current env.
 **/
void reboot(int cmd);

/**
 * Attempt to reboot the machine.
 **/
int reboot_global(int cmd);

enum {
    REBOOT_NORMAL,
    REBOOT_NORETURN
};

#endif