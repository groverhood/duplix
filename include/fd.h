#ifndef _FD_H_
#define _FD_H_

#include <dev.h>
#include <stddef.h>
#include <dev/blkdev.h>

typedef int fdtype_t;

enum {
    FD_SPECIAL,
    FD_NET,
    FD_DEV,
    FD_FS,
    FD_TYPE_COUNT
};

typedef int64_t off_t;
typedef uint64_t ino_t;
typedef uint64_t nlink_t;
typedef int mode_t;
typedef size_t blksize_t;
typedef size_t blkcnt_t;

struct stat {
    dev_t st_dev;
    ino_t st_ino;
    mode_t st_mode;
    nlink_t st_nlink;
    uid_t st_uid;
    gid_t st_gid;
    dev_t st_rdev;
    off_t st_size;
    blksize_t st_blksize;
    blkcnt_t st_blocks;
};

struct fd_extent {
    blkaddr_t ext_base;
    blkcnt_t ext_blks;
    int ext_flags;
};

struct fd_shared {
    /* Since accesses are minimized to O(1) accesses, it is better to just use
       a spinlock here as it avoids any traps. */
    spinlock_t fds_lock;
    struct stat fds_stat;
    uint8_t fds_data[0];
};

#define FDBASEOFS (offsetof(struct fd_shared, fds_##data))
#define FDEXTSZ (BLKSIZ - FDBASEOFS)

struct fd;

typedef size_t (*fdwrite_t)(struct fd *fd, const void *buf, size_t size);
typedef size_t (*fdread_t)(struct fd *fd, void *buf, size_t size);
typedef int (*fdclose_t)(struct fd *fd);
typedef int (*fdflush_t)(struct fd *fd);

/* TODO: add locked I/O */
struct fd {
    struct fd_shared *fd_shared;
    struct dev fd_dev;
    int fd_mode;
    int fd_num;
    char *fd_buf;
    size_t fd_bufsiz;
    size_t fd_blkbase;
    size_t fd_ofs;
    size_t fd_pos;
    size_t fd_nread;
    fdwrite_t fd_write;
    fdread_t fd_read;
    fdclose_t fd_close;
    fdflush_t fd_flush;
};

enum { FDNUM_BITS = 30 };
#define fdnumtype(fdnum) ((fdnum) >> FDNUM_BITS)
#define fdnumidx(fdnum) ((fdnum) & ((1 << FDNUM_BITS) - 1))

int fdnum_alloc(fdtype_t type);
struct fd *fdnum2fd(int fdnum);
size_t genericwrite(struct fd *fd, const void *buf, size_t bytes);

static inline int fdclose(struct fd *fd)
{
    return fd->fd_close(fd);    
}

static inline int fdflush(struct fd *fd)
{
    return fd->fd_flush(fd);
}

static inline size_t fdread(struct fd *fd, void *buf, size_t size)
{
    return fd->fd_read(fd, buf, size);
}

static inline size_t fdwrite(struct fd *fd, const void *buf, size_t size)
{
    return fd->fd_write(fd, buf, size);
}

#endif