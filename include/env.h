#ifndef _ENV_H_
#define _ENV_H_

#include <user.h>
#include <proc.h>
#include <sysarg.h>
#include <kerninst.h>

typedef int envid_t;
typedef int envtype_t;

enum {
    /**
     * Host kernel. Pretty fast, pretty safe, but you can never be too sure.
     **/
    ENV_TYPE_HOST,
    /**
     * Hardware virtualized kernel. Slower, but extra safe.
     **/
    ENV_TYPE_GUEST,
    /**
     * I/O Devices have entire kernels dedicated towards them to ensure 
     * complete isolation from buggy processes and daft user programs.
     **/
    ENV_TYPE_DEVICE,
};

/* Environment structure shared between the userspace and the kernel. */
struct env {
    uid_t e_owner;        // >>> User who owns the env, aka root 
    envid_t e_next;       // >>> Next env to schedule
    envid_t e_self;       // >>> Env UUID
    envtype_t e_type;     // >>> Env type
    atomic_int e_trapped; // >>> Are interrupts "disabled"?
    procid_t e_curproc;   // >>> Pointer to the current process
    procid_t e_man;       // >>> Env management process
    procid_t e_fs;        // >>> Filesystem server process
    procid_t e_ns;        // >>> Network server process
    procid_t e_devs;      // >>> Device server process
    procid_t e_dyns;      // >>> Dynamic image server process
};

#define ENVSIZ (PGSIZE * ((sizeof(struct env) + PGSIZE - 1) / PGSIZE))

struct imagehdr;

extern struct env *curenv;

/* Create a new env */
envid_t env_raise(struct imagehdr *hdr);
/* Schedule an env to run on an instance of the Duplix kernel */
int env_sched(envid_t env, kerninst_t inst);
/* Destroy an env */
int env_shutdown(envid_t env);
/* Map an env object into memory */
int env_map(envid_t env, struct env **out_env);

#endif