#ifndef _FS_H_
#define _FS_H_

#include <fd.h>
#include <dev.h>
#include <pmap.h>
#include <user.h>
#include <errno.h>
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include <nokernel.h>

enum /* open_flags */ {
    O_APPEND    = (1 << 0),
    O_ASYNC     = (1 << 1),
    O_CLOEXEC   = (1 << 2),
    O_CREAT     = (1 << 3),
    O_DIRECT    = (1 << 4),
    O_DIRECTORY = (1 << 5),
    O_DSYNC     = (1 << 6),
    O_EXCL      = (1 << 7),
    O_LARGEFILE = (1 << 8),
    O_NOATIME   = (1 << 9),
    O_NOCTTY    = (1 << 10),
    O_NOFOLLOW  = (1 << 11),
    O_NONBLOCK  = (1 << 12),
    O_PATH      = (1 << 13),
    O_SYNC      = (1 << 14),
    O_TMPFILE   = (1 << 15),
};

enum {
    FSIPC_OPEN2,
    FSIPC_OPEN3,
    FSIPC_CREAT,
    FSIPC_CREATE = FSIPC_CREAT,
    FSIPC_UNLINK,
    FSIPC_CHMOD,
    FSIPC_STAT,
    FSIPC_COUNT
};

enum {
    UNIQFD_STDOUT,
    UNIQFD_STDIN,
    UNIQFD_STDERR,
    UNIQFD_COUNT
};

nokernel int open3(const char *pathname, int flags, mode_t mode);
nokernel int open2(const char *pathname, int flags);

static inline nokernel int open(const char *pathname, int flags, ...)
{
    int r;
    mode_t mode;
    va_list rest;
    if (flags & (O_CREAT | O_TMPFILE)) {
        va_start(rest, flags);
        mode = va_arg(rest, mode_t);
        r = open3(pathname, flags, mode);
        va_end(rest);
    } else {
        r = open2(pathname, flags);
    }
    return r;
}

/* This function is deprecated because its name is awful. Use
   create(3) instead. */
nokernel int creat(const char *pathname, mode_t mode);

static inline nokernel int create(const char *pathname, mode_t mode)
{
    return creat(pathname, mode);
}

nokernel int link(const char *oldpath, const char *newpath);

static inline nokernel int close(int fd)
{
    struct fd *usrfd;
    usrfd = fdnum2fd(fd);
    if (usrfd == NULL) {
        return -EINVAL;
    }
    return usrfd->fd_close(usrfd);
}

nokernel int unlink(const char *pathname);

nokernel int chmod(const char *pathname, mode_t mode);
static inline nokernel int fchmod(int fd, mode_t mode)
{
    if (fd < 0 || fdnumtype(fd) != FD_FS) {
        return -EINVAL;
    }
    struct fd *usrfd = fdnum2fd(fd);
    struct fd_shared *shared = usrfd->fd_shared;
    spinlock_acquire(&shared->fds_lock);
    shared->fds_stat.st_mode = mode;
    spinlock_release(&shared->fds_lock);
    return 0;
}

static inline nokernel long read(int fd, void *dst, size_t bytes)
{
    struct fd *usrfd;
    usrfd = fdnum2fd(fd);
    if (usrfd == NULL) {
        return -EINVAL;
    }
    return (long)usrfd->fd_read(usrfd, dst, bytes);
}

static inline nokernel long write(int fd, const void *src, size_t bytes)
{
    struct fd *usrfd;
    usrfd = fdnum2fd(fd);
    if (usrfd == NULL) {
        return -EINVAL;
    }
    return (long)usrfd->fd_write(usrfd, src, bytes);
}

nokernel int stat(const char *filename, struct stat *out_stat);
static inline nokernel int fstat(int fd, struct stat *out_stat)
{
    if (fd < 0 || fdnumtype(fd) != FD_FS || out_stat == NULL) {
        return -EINVAL;
    }
    struct fd *usrfd = fdnum2fd(fd);
    struct fd_shared *shared = usrfd->fd_shared;
    spinlock_acquire(&shared->fds_lock);
    memcpy(out_stat, &shared->fds_stat, sizeof *out_stat);
    spinlock_release(&shared->fds_lock);
    return 0;
}

nokernel int fallocate(int fd, off_t offset, off_t len);

#endif