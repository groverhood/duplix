#ifndef _STDATOMIC_H_
#define _STDATOMIC_H_

#include <stdbool.h>

#define atomic _Atomic

#define atomic_load(ptr) __atomic_load_n(ptr, __ATOMIC_SEQ_CST)
#define atomic_store(ptr, val) __atomic_store_n(ptr, val, __ATOMIC_SEQ_CST)
#define atomic_test_and_set(ptr) __atomic_test_and_set(ptr, __ATOMIC_SEQ_CST)

#define atomic_fetch_add(ptr, val) __atomic_fetch_add(ptr, val, __ATOMIC_SEQ_CST)

#define atomic_init(ptr, val) atomic_store(ptr, val)

typedef atomic bool atomic_bool;
typedef atomic int atomic_int;
typedef atomic size_t atomic_size_t;

#endif