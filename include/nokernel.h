#ifndef _NOKERNEL_H_
#define _NOKERNEL_H_

/**
 *  nokernel is a keyword used for ensuring that user-only functions that
 *  MUST not be used in the kernel but find themselves being invoked can
 *  raise an error to alert the developer to their faulty usage.
 **/
#ifdef __KERNEL__
#define nokernel __attribute__((error ("This function should not be invoked in the kernel")))
#else
#define nokernel
#endif

#endif