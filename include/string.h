#ifndef _STRING_H_
#define _STRING_H_

#include <stddef.h>

void *memcpy(void *dst, const void *src, size_t bytes);
void *memset(void *dst, int val, size_t bytes);

char *strchr(const char *s, int c);

int strcmp(const char *s1, const char *s2);

#endif