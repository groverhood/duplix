#ifndef _LIST_H_
#define _LIST_H_

#include <stddef.h>
#include <spinlock.h>

struct list_elem {
    struct list_elem *el_prev;
    struct list_elem *el_next;
};

#define list_elem(s,m,e) ((s *)((uint8_t *)(e) - offsetof(s,m)))

static inline void list_elem_init(struct list_elem *e)
{
    e->el_prev = NULL;
    e->el_next = NULL;
}

struct list {
    struct list_elem *li_head;
    struct list_elem *li_tail;
    struct list_elem li_endtag;
    size_t li_size;  
    spinlock_t li_lock;
};

static inline void list_init(struct list *li)
{
    li->li_head = NULL;
    li->li_tail = NULL;
    list_elem_init(&li->li_endtag);
    li->li_size = 0;
    spinlock_init(&li->li_lock);
}

static inline void list_add(struct list_elem *li, struct list_elem *e)
{
    if (e->el_prev != NULL) {
        e->el_prev->el_next = e->el_next;
    }
    if (e->el_next != NULL) {
        e->el_next->el_prev = e->el_prev;   
    }
    if (li->el_next != NULL) {
        li->el_next->el_prev = e;
    }
    e->el_next = li->el_next;
    e->el_prev = li;
    li->el_next = e;
}

static struct list_elem *list_remove(struct list_elem *li)
{
    if (li->el_prev != NULL) {
        li->el_prev->el_next = li->el_next;
    }
    if (li->el_next != NULL) {
        li->el_next->el_prev = li->el_prev;
    }
    li->el_next = NULL;
    li->el_prev = NULL;
    return li;
}

static struct list_elem *list_next(struct list_elem *e)
{
    return (e == NULL) ? NULL : e->el_next;
}

static struct list_elem *list_begin(struct list *li)
{
    return li->li_head;
}

static struct list_elem *list_end(struct list *li)
{
    return &li->li_endtag;
}

static inline int list_null(struct list *li)
{
    return (li->li_size == 0);
}

static inline void list_push(struct list *li, struct list_elem *e)
{
    spinlock_acquire(&li->li_lock);
    struct list_elem *endtag = list_end(li);
    if (li->li_size == 0) {
        li->li_head = e; 
    } else  {
        list_add(li->li_tail, e);
    }
    li->li_tail = e;
    endtag->el_prev = e;
    e->el_next = endtag;
    li->li_size++;
    spinlock_release(&li->li_lock);
}

static inline struct list_elem *list_pop(struct list *li)
{
    struct list_elem *e = NULL;
    spinlock_acquire(&li->li_lock);
    if (li->li_size > 0) {
        e = list_remove(li->li_head);
        li->li_size--;
        li->li_head = li->li_head->el_next;
    }
    spinlock_release(&li->li_lock);
    return e;
}

static inline void list_delete(struct list *li, struct list_elem *e)
{
    spinlock_acquire(&li->li_lock);
    if (li->li_size > 0) {
        if (e == li->li_head) {
            li->li_head = li->li_head->el_next;
        }
        if (e == li->li_tail) {
            li->li_tail = li->li_tail->el_prev;
        }
        list_remove(e);
        li->li_size--;
    }
    spinlock_release(&li->li_lock);
}

#endif