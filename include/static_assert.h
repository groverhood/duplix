#ifndef _STATIC_ASSERT_H_
#define _STATIC_ASSERT_H_

#ifndef __cplusplus
#define static_assert(cond,error) _Static_assert(cond,error)
#endif

#endif