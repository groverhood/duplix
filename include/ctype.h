#ifndef _CTYPE_H_
#define _CTYPE_H_

#define isspace(c) ((c) == ' ' || \
                    (c) == '\t' || \
                    (c) == '\n' || \
                    (c) == '\r' || \
                    (c) == '\f')
#define isdigit(c) ('0' <= (c) && (c) <= '9')

#endif