#ifndef _PGFLAGS_H_
#define _PGFLAGS_H_

#include <stdint.h>

/**
 *  GENERAL MEMORY LAYOUT
 *  [0x0,                0x1000000         ] -- Memory hole
 *  [0x1000000,          0x1002000         ] -- PCB and ECB mappings
 *  [0x1002000,          0x7fffffffffff    ] -- Memory available for userland
 *  [0x800000000000,     0xffff7fffffffffff] -- Memory hole
 *  [0xffff800000000000, 0xffffffff7fffffff] -- Memory available for kernel
 *  [0xffffffff80000000, 0xffffffffefffffff] -- Kernel I/O region
 *  [0xfffffffff0000000, 0xffffffffffffffff] -- Memory hole
 **/

enum {
    PG_PGTABLE,
    PG_PGDIR,
    PG_PDPT,
    PG_PML4
};

#define UBOT (0x100000 * 16) // 16 MiB
#define UTOP 0x800000000000
#define KBASE (UTOP | ((uint64_t)0xffff << 48))

/* ===== HARDWARE PAGE TABLE FLAGS ===== */

#define PG_OFFBITS 12
#define PG_IDXBITS 9
#define PG_IDXMASK ((1 << 9) - 1)
#define PG_FLAGS ((1 << PG_OFFBITS) - 1)
#define PG_ADDR (~PG_FLAGS)
#define PG_ENTRIES (1 << 9)

#define PHYSMASK (~(((uint64_t)0xfff << 48) | 0xfff))
#define PGSIZE (1 << PG_OFFBITS)
#define HUGESIZE (1 << (PG_OFFBITS + PG_IDXBITS))
#define GIGASIZE (1 << (PG_OFFBITS + PG_IDXBITS * 2))

#define PTE_P    (1 << 0)
#define PTE_RDWR (1 << 1)
#define PTE_USER (1 << 2)
#define PTE_PWT  (1 << 3)
#define PTE_UC   (1 << 4)
#define PTE_A    (1 << 5)
#define PTE_D    (1 << 6)
#define PTE_PS   (1 << 7)
#define PTE_G    (1 << 8)

#define PGINDEX(va, level) (((uintptr_t)(va) >> ((level) * PG_IDXBITS + PG_OFFBITS)) & PG_IDXMASK)
#define PGENTRY(pa, flags) ((uintptr_t)(pa) | (flags))

/* ===== SOFTWARE PAGE TABLE FLAGS ===== */

/**
 * |   Node VA   | 1 |
 * |0000000000000|000|
 * 
 * 1. |         NWP|
 *    |000000000000|
 **/

#define VPT_PRESENT   (1 << 0)
#define VPT_WRITEABLE (1 << 1)
#define VPT_NODE      (1 << 2)

#define VPT_FLGBITS 12
#define VPT_FLAGS ((1 << VPT_FLGBITS) - 1)
#define VPT_ADDR (~VPT_FLAGS)

#endif