#ifndef _FENCE_H_
#define _FENCE_H_

#define fence asm volatile ("mfence" ::: "memory")

#endif