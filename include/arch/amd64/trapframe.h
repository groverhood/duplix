#ifndef _AMD64_TRAPFRAME_H_
#define _AMD64_TRAPFRAME_H_

#include <stdint.h>
#include <static_assert.h>

struct pushregs {
    uint64_t rg_rax;
    uint64_t rg_rbx;
    uint64_t rg_rcx;
    uint64_t rg_rdx;
    uint64_t rg_rdi;
    uint64_t rg_rsi;
    uint64_t rg_r8;
    uint64_t rg_r9;
    uint64_t rg_r10;
    uint64_t rg_r11;
    uint64_t rg_r12;
    uint64_t rg_r13;
    uint64_t rg_r14;
    uint64_t rg_r15;
    uint64_t rg_rbp;
};

struct trapframe {
    /* Software frame  -- 144 bytes */
    struct pushregs tf_regs;
    uint16_t tf_es;
    uint16_t _pad0[3];
    uint16_t tf_ds;
    uint16_t _pad1[3];
    uint64_t tf_trapno;
    /* Interrupt frame -- 32 bytes */
    uint64_t tf_err;
    uint64_t tf_rip;
    uint16_t tf_cs;
    uint16_t _pad2[3];
    uint64_t tf_rflags;
    /* Crossing rings  -- 16 bytes */
    uint64_t tf_rsp;
    uint16_t tf_ss;
    uint16_t _pad3[3];
    /* End trapframe */
};

#define ldregs(p_regs) \
    asm volatile ("movq %%rax, %P[rax](%[regs])\n\t" \
                  "movq %%rbx, %P[rbx](%[regs])\n\t" \
                  "movq %%rcx, %P[rcx](%[regs])\n\t" \
                  "movq %%rdx, %P[rdx](%[regs])\n\t" \
                  "movq %%rdi, %P[rdi](%[regs])\n\t" \
                  "movq %%rsi, %P[rsi](%[regs])\n\t" \
                  "movq %%r8,  %P[r8](%[regs])\n\t"  \
                  "movq %%r9,  %P[r9](%[regs])\n\t"  \
                  "movq %%r10, %P[r10](%[regs])\n\t" \
                  "movq %%r11, %P[r11](%[regs])\n\t" \
                  "movq %%r12, %P[r12](%[regs])\n\t" \
                  "movq %%r13, %P[r13](%[regs])\n\t" \
                  "movq %%r14, %P[r14](%[regs])\n\t" \
                  "movq %%r15, %P[r15](%[regs])\n\t" \
                  "movq %%rbp, %P[rbp](%[regs])\n\t" \
                :                                    \
                : [rax] "i" (offsetof(struct pushregs, rg_rax)), \
                  [rbx] "i" (offsetof(struct pushregs, rg_rbx)), \
                  [rcx] "i" (offsetof(struct pushregs, rg_rcx)), \
                  [rdx] "i" (offsetof(struct pushregs, rg_rdx)), \
                  [rdi] "i" (offsetof(struct pushregs, rg_rdi)), \
                  [rsi] "i" (offsetof(struct pushregs, rg_rsi)), \
                  [r8]  "i" (offsetof(struct pushregs, rg_r8)),  \
                  [r9]  "i" (offsetof(struct pushregs, rg_r9)),  \
                  [r10] "i" (offsetof(struct pushregs, rg_r10)), \
                  [r11] "i" (offsetof(struct pushregs, rg_r11)), \
                  [r12] "i" (offsetof(struct pushregs, rg_r12)), \
                  [r13] "i" (offsetof(struct pushregs, rg_r13)), \
                  [r14] "i" (offsetof(struct pushregs, rg_r14)), \
                  [r15] "i" (offsetof(struct pushregs, rg_r15)), \
                  [rbp] "i" (offsetof(struct pushregs, rg_rbp)), \
                  [regs] "r" (p_regs))

#define stregs(pregs) \
    asm volatile ("movq %P[rax](%[regs]), %%rax\n\t" \
                  "movq %P[rbx](%[regs]), %%rax\n\t" \
                  "movq %P[rcx](%[regs]), %%rcx\n\t" \
                  "movq %P[rdx](%[regs]), %%rdx\n\t" \
                  "movq %P[rdi](%[regs]), %%rdi\n\t" \
                  "movq %P[rsi](%[regs]), %%rsi\n\t" \
                  "movq %P[r8](%[regs]),  %%r8\n\t"  \
                  "movq %P[r9](%[regs]),  %%r9\n\t"  \
                  "movq %P[r10](%[regs]), %%r10\n\t" \
                  "movq %P[r11](%[regs]), %%r11\n\t" \
                  "movq %P[r12](%[regs]), %%r12\n\t" \
                  "movq %P[r13](%[regs]), %%r13\n\t" \
                  "movq %P[r14](%[regs]), %%r14\n\t" \
                  "movq %P[r15](%[regs]), %%r15\n\t" \
                  "movq %P[rbp](%[regs]), %%rbp\n\t" \
                :                                    \
                : [rax] "i" (offsetof(struct pushregs, rg_rax)), \
                  [rbx] "i" (offsetof(struct pushregs, rg_rbx)), \
                  [rcx] "i" (offsetof(struct pushregs, rg_rcx)), \
                  [rdx] "i" (offsetof(struct pushregs, rg_rdx)), \
                  [rdi] "i" (offsetof(struct pushregs, rg_rdi)), \
                  [rsi] "i" (offsetof(struct pushregs, rg_rsi)), \
                  [r8]  "i" (offsetof(struct pushregs, rg_r8)),  \
                  [r9]  "i" (offsetof(struct pushregs, rg_r9)),  \
                  [r10] "i" (offsetof(struct pushregs, rg_r10)), \
                  [r11] "i" (offsetof(struct pushregs, rg_r11)), \
                  [r12] "i" (offsetof(struct pushregs, rg_r12)), \
                  [r13] "i" (offsetof(struct pushregs, rg_r13)), \
                  [r14] "i" (offsetof(struct pushregs, rg_r14)), \
                  [r15] "i" (offsetof(struct pushregs, rg_r15)), \
                  [rbp] "i" (offsetof(struct pushregs, rg_rbp)), \
                  [regs] "r" (pregs))

static_assert(sizeof(struct trapframe) == 144 + 32 + 16, "Trapframe is improperly aligned");

#endif