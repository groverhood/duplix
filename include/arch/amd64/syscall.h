#ifndef _AMD64_SYSCALL_H_
#define _AMD64_SYSCALL_H_

#include <stdatomic.h>
#include <sysarg.h>
#include <syscn.h>
#include <env.h>

static inline sysarg_t sys_cnum2arg(syscnum_t cnum)
{
    return (sysarg_t)cnum;
}

static inline syscnum_t sys_arg2cnum(sysarg_t arg)
{
    return (syscnum_t)arg;
}

static inline int _syscall(sysarg_t rax, sysarg_t arg0, sysarg_t arg1, 
                           sysarg_t arg2, sysarg_t arg3, sysarg_t arg4,
                           sysarg_t arg5)
{
    int ret;
    struct trapframe *tf;
    register void *volatile rsp asm("rsp");
    /* Set this to true first so that we don't schedule other processes */
    atomic_store(&curenv->e_trapped, true);
    tf = &curproc->pr_tf;
    /* Load all general purpose registers */
    ldregs(&tf->tf_regs);
    /* Clobber registers */
    tf->tf_regs.rg_rax = rax;
    tf->tf_regs.rg_rdi = arg0;
    tf->tf_regs.rg_rsi = arg1;
    tf->tf_regs.rg_rdx = arg2;
    tf->tf_regs.rg_rcx = arg3;
    tf->tf_regs.rg_r8  = arg4;
    tf->tf_regs.rg_r9  = arg5;
    tf->tf_trapno      = rax;
    tf->tf_rip         = (uint64_t)&&epilogue;
    tf->tf_rsp         = (uint64_t)rsp;
    asm volatile ("syscall" :: "D" (tf));
epilogue:
    ret = tf->tf_regs.rg_rax;
    atomic_store(&curenv->e_trapped, false);
    return ret;
}

#define syscall6(cn, arg0, arg1, arg2, arg3, arg4, arg5) _syscall(cn, arg0, arg1, arg2, arg3, arg4, arg5)
#define syscall5(cn, arg0, arg1, arg2, arg3, arg4) _syscall(cn, arg0, arg1, arg2, arg3, arg4, 0)
#define syscall4(cn, arg0, arg1, arg2, arg3) _syscall(cn, arg0, arg1, arg2, arg3, 0, 0)
#define syscall3(cn, arg0, arg1, arg2) _syscall(cn, arg0, arg1, arg2, 0, 0, 0)
#define syscall2(cn, arg0, arg1) _syscall(cn, arg0, arg1, 0, 0, 0, 0)
#define syscall1(cn, arg0) _syscall(cn, arg0, 0, 0, 0, 0, 0)
#define syscall0(cn) _syscall(cn, 0, 0, 0, 0, 0, 0)

#endif