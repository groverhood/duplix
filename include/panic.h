#ifndef _PANIC_H_
#define _PANIC_H_

#include <crt.h>
#include <stdio.h>
#include <reboot.h>

#define __stringify__(m) #m
#define stringify(m) __stringify__(m)

#define panic(format, ...) do { \
    printf("PANIC!!!\n[%s] panicked at " __FILE__ ":" stringify(__LINE__) "\nREASON: " format "\n", process, ##__VA_ARGS__); \
    reboot(REBOOT_NORETURN); \
} while (0)

#endif