#ifndef _THREAD_H_
#define _THREAD_H_

#include <crt.h>
#include <list.h>
#include <event.h>
#include <spinlock.h>

#define thread_local __thread

typedef int threadid_t;

struct thread_mutex {
    struct list mut_queue;
    spinlock_t mut_guard;
    atomic_int mut_held;
};

void mutex_init(struct thread_mutex *lock);
void mutex_lock(struct thread_mutex *lock);
void mutex_unlock(struct thread_mutex *lock);

/* For shared_mutex */
struct wakeup_event_packet {
    PACKET_HEADER;
    threadid_t wk_target;
};

struct thread_shared_mutex {
    size_t shm_ringbuflen;
    size_t shm_ringbufpos;
    spinlock_t shm_guard;
    int shm_held;
    volatile threadid_t shm_ringbuf[0];
};

void shmutex_init(struct thread_shared_mutex *lock, size_t minthreads);
void shmutex_lock(struct thread_shared_mutex *lock);
void shmutex_unlock(struct thread_shared_mutex *lock);

struct thread_sem {
    spinlock_t sem_guard;   
    int sem_value;
    struct list sem_queue; 
};

void sem_init(struct thread_sem *sem);
void sem_up(struct thread_sem *sem);
void sem_down(struct thread_sem *sem);

typedef int (*threadfun_t)(void *context);

struct thread {
    struct list_elem thr_elem;
    void *thr_context;
    int thr_status;
    threadfun_t thr_entry;
    threadid_t thr_id;
};

extern struct thread *curthread;

ctor int init_threads(void);

struct thread *create_thread(threadfun_t func, void *context);
int thread_sched(struct thread *thr);
int thread_kill(struct thread *thr);
void thread_block(struct thread *thr);
struct thread *id2thread(threadid_t id);

int thread_yield(void);
void thread_exit(void);

#endif