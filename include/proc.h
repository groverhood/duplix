#ifndef _PROC_H_
#define _PROC_H_

#include <list.h>
#include <stdint.h>
#include <stdbool.h>
#include <trapframe.h>

typedef int procid_t;

struct env;
struct pmap_state;

enum {
    PROCID_NULL = -1, // >>> Nonexistent process
    PROCID_ENV,       // >>> Current environment manager
    PROCID_SELF,      // >>> Current process
};

/* May be mapped into memory by foreign processes and envs */
#define MAP
/* May not be mapped into memory by foreign processes and envs */
#define NOMAP

#define NAMELEN 64

/* Process structure shared between the userspace and the kernel */
struct proc {
    struct trapframe pr_tf;    // >>> Process trapframe
    procid_t pr_self;          // >>> Process UID
    MAP struct env *pr_env;    // >>> Pointer to environment struct
    bool pr_isenv;             // >>> Set when the process is the current environment manager

    NOMAP int pr_ptroot;       // >>> Page table root capability
    MAP struct pmap_state 
                *pr_pager;     // >>> Process paging state
    int pr_status;             // >>> Exit status
    char pr_execname[NAMELEN]; // >>> Basename of the executable image this process is based on
    NOMAP char *pr_argpage;    // >>> Page containing argv[] contents separated by null terminators

    NOMAP void *pr_pfupcall;   // >>> Process VA pointing to page fault callback    
    NOMAP void *pr_evthandler; // >>> Process VA pointing to event handler
    NOMAP void *pr_tmrhandler; // >>> Process VA pointing to the timer interrupt upcall
    NOMAP void *pr_stack;      // >>> Process stack pointer

    procid_t pr_serialep;      // >>> Serial endpoint process
    NOMAP struct list 
                  pr_events;   // >>> Event queue
    
    NOMAP struct list_elem 
                     pr_el;    // >>> Scheduling list element
};

#define PROCSZ (PGSIZE * ((sizeof(struct proc) + PGSIZE - 1) / PGSIZE))

extern struct proc *curproc;

enum {
    PROCMAP_PAGER,
    PROCMAP_ENV,
};

/* Map a process struct into memory */
int proc_map(procid_t proc, int flags, struct proc **out_proc);
/* Schedule the next process */
int proc_sched(procid_t next);
/* Yield to the next process in the queue */
int proc_yield(void);
/* Block the current process */
int proc_block(void);
/* Unblock a process */
int proc_unblock(procid_t proc);

#endif