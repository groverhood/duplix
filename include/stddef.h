#ifndef _STDDEF_H_
#define _STDDEF_H_

typedef __SIZE_TYPE__ size_t;

#define SIZE_MAX __SIZE_MAX__

typedef __PTRDIFF_TYPE__ ptrdiff_t;

#define PTRDIFF_SIZE __PTRDIFF_SIZE__

#ifndef __cplusplus
#define NULL (void *)0
#else
#define NULL 0
#endif

#define offsetof(s,m) __builtin_offsetof(s,m)

#endif