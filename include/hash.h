#ifndef _HASH_H_
#define _HASH_H_

#include <stddef.h>

struct hash;
struct list_elem;

typedef size_t (*hashfunc_t)(void *key);

struct hash *mkhash(void);

int hashins(struct hash *hash, void *key, struct list_elem *val);
struct list_elem *hashlkup(struct hash *hash, void *key);
int hashdel(struct hash *hash, void *key);

size_t hashsize(struct hash *hash);

size_t hashi(void *ptr);
size_t hashs(void *ptr);
size_t hashi32(void *ptr);
size_t hashu32(void *ptr);
size_t hashi64(void *ptr);
size_t hashu64(void *ptr);

#endif