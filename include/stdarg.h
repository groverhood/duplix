#ifndef _STDARG_H_
#define _STDARG_H_

typedef __builtin_va_list va_list;

#define va_start(p,a) __builtin_va_start(p,a)
#define va_arg(p,t) __builtin_va_arg(p,t)
#define va_end(p) __builtin_va_end(p)

#endif