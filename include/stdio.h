#ifndef _STDIO_H_
#define _STDIO_H_

#include <fs.h>
#include <serial.h>
#include <stdarg.h>
#include <stddef.h>
#include <nokernel.h>

struct fd;
typedef struct fd FILE;

extern FILE *stdout;
extern FILE *stdin;
extern FILE *stderr;

int printf(const char *format, ...);
int sprintf(char *dest, const char *format, ...);
int snprintf(char *dest, size_t size, const char *format, ...);

int vprintf(const char *format, va_list args);
int vsprintf(char *dest, const char *format, va_list args);
int vsnprintf(char *dest, size_t size, const char *format, va_list args);

nokernel int vfprintf(FILE *stream, const char *format, va_list args);

static inline nokernel int fprintf(FILE *stream, const char *format, ...)
{
    int r;
    va_list p;
    va_start(p, format);
    r = vfprintf(stream, format, p);
    va_end(p);
    return r;
}

static inline nokernel int vdprintf(int fd, const char *format, va_list args)
{
    return vfprintf(fdnum2fd(fd), format, args);
}

static inline nokernel int dprintf(int fd, const char *format, ...)
{
    int r;
    va_list p;
    va_start(p, format);
    r = vdprintf(fd, format, p);
    va_end(p);
    return r;
}

/* Parse a string used as the mode parameter in fopen(3). Returns either
   a negated errno constant or the fd_mode value were it to be stored in
   a file stream object. */
nokernel int parsemode(const char *modestr, int *flags, mode_t *mode);

static inline nokernel FILE *fopen(const char *filepath, const char *modes)
{
    int flags;
    mode_t mode;
    if (parsemode(modes, &flags, &mode) < 0) {
        return NULL;
    }
    return (FILE *)fdnum2fd(open(filepath, flags, mode));
}

static inline nokernel FILE *fdopen(int fd, const char *modes)
{
    int mode = parsemode(modes, NULL, NULL);
    if (mode < 0) {
        return NULL;
    }
    FILE *stream = (FILE *)fdnum2fd(fd);
    if (stream->fd_mode != mode) {
        return NULL;
    }
    return stream;
}

static inline nokernel int fclose(FILE *stream)
{
    return fdclose((struct fd *)stream);
}

static inline nokernel int fflush(FILE *stream)
{
    return fdflush((struct fd *)stream);
}

static inline nokernel size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    return fdread((struct fd *)stream, ptr, size * nmemb);    
}

static inline nokernel size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    return fdwrite((struct fd *)stream, ptr, size * nmemb);    
}

static inline nokernel int fputc(int c, FILE *stream)
{
    return (int)stream->fd_write(stream, &c, sizeof c);    
}

static inline nokernel int fputs(const char *s, FILE *stream)
{
    int r;
    int cnt = 0;
    while (*s) {
        r = fputc(*s++, stream);
        if (r < 0) {
            return r;
        }
        cnt++;
    }
    return cnt;
}

static inline nokernel int putc(int c, FILE *stream)
{
    return fputc(c, stream);
}

#endif