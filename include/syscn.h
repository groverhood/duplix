#ifndef _SYSCN_H_
#define _SYSCN_H_

typedef int syscnum_t;

enum {
    SYS_SEND_EVENT,
    SYS_CREATE_PROC,
    SYS_SCHED_PROC,
    SYS_KILL_PROC,
    SYS_MAP_PROC,
    SYS_CREATE_ENV, 
    SYS_SCHED_ENV,
    SYS_KILL_ENV,
    SYS_MAP_ENV,
    SYS_PAGE_MAP,
    SYS_PAGE_UNMAP,
    /* Debug syscalls -- do nothing in non-development/testing builds */
    SYS_PRINT,
    SYSCALL_COUNT
};

#endif