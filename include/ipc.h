#ifndef _IPC_H_
#define _IPC_H_

#include <env.h>
#include <proc.h>
#include <pmap.h>
#include <event.h>
#include <thread.h>
#include <stddef.h>
#include <stdarg.h>
#include <static_assert.h>

struct ipc_shared {
    bool shr_dirty;
    spinlock_t shr_guard;
    size_t shr_length;
    uint8_t shr_start[0];
};

static inline void *ipc_shmem(struct ipc_shared *shr)
{
    return shr->shr_start;
}

struct ipc {
    struct ipc_shared *ipc_buffer;
    struct thread_mutex ipc_mtx;
    envid_t ipc_env;
    procid_t ipc_proc;
    int ipc_waiting;
};

struct ipc_init_packet {
    PACKET_HEADER;
    void *pld_sharedva;
    size_t pld_sharedpages;
};

int ipc_create(envid_t env, procid_t proc, struct ipc *out_ipc);
void ipc_free(struct ipc *ipc);
long ipc_write(const void *src, size_t bytes, struct ipc *ep);
long ipc_wrargv(struct ipc *ep, const char *format, va_list args);

static inline long ipc_wrarg(struct ipc *ep, const char *format, ...)
{
    long r;
    va_list args;
    va_start(args, format);
    r = ipc_wrargv(ep, format, args);
    va_end(args);
    return r;
}

struct ipc_signal_packet {
    PACKET_HEADER;
};

static inline int ipc_signal(struct ipc *ep, int wait)
{
    int r;
    int status;
    struct thread_sem sem;
    struct thread_sem *psem;
    struct ipc_signal_packet pkt = { PACKET_INIT(EVT_IPC_SIGNAL, &status, psem) };
    psem = NULL;
    if (wait) {
        psem = &sem;
        sem_init(psem);
    }
    r = sendevt(ep->ipc_env, ep->ipc_proc, &pkt.evt_pkt);
    if (r < 0) {
        return r;
    }
    if (wait) {
        sem_down(psem);
    }
    if (status < 0) {
        return status;
    }
    return 0;
}

long ipc_read(void *dest, size_t bytes, struct ipc *ep);

#endif