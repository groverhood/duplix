#ifndef _CPU_H_
#define _CPU_H_

#include <env.h>
#include <cpustat.h>

typedef int cpu_t;

enum { BSPCPU };

int cpucount(void);
int cpustat(cpu_t cpuid, struct cpustat *statbuf);

#endif