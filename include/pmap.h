#ifndef _PMAP_H_
#define _PMAP_H_

#include <crt.h>
#include <env.h>
#include <proc.h>
#include <thread.h>
#include <pgflags.h>

/* Address used for mapping the PCB to its own page table */
#define UPROC ((void *)UBOT)
/* Address used for mapping the ECB to the page tabel of curproc */
#define UENV ((void *)((uintptr_t)UPROC + PROCSZ))
/* Value to initialize the main thread's stack pointer with */
#define USTKTOP ((void *)UTOP)

/**
 *   |-- Node VA --| F |
 * 0x[0000000000000|000]
 **/
typedef uint64_t pmapent_t;

#define PMAP_FLAGS ((uint64_t)0xfff)
#define PMAP_ADDR (~PMAP_FLAGS)

void handle_pagefault(struct trapframe *tf, void *va);

/**
 *  Paging state structure. Over time it forms a table of
 *  its own that shadows the real page table, allowing for
 *  minimal trapping while self-paging.
 **/
struct pmap_state {
    pmapent_t *pm_vptrt;
    void *pm_base;
    uint8_t *pm_bump;
    size_t pm_size;
};

int pmap_init(void);
struct pmap_state *get_pmap_state(void);
struct pmap_state *create_foreign_pmap(envid_t env, procid_t proc);

/**
 *  Data stored at the bottom level of the virtual page
 *  table.
 **/
struct pmap_node {
    int pn_mapped;
    int pn_perm;
    size_t pn_npages;
    pmapent_t *pn_vpt;
    struct list_elem pn_elem;
    void *pn_va;
};

struct pmap_node *pmap_lookup(struct pmap_state *state, void *va);
struct pmap_node *pmap_reserve(struct pmap_state *state, void *va, size_t npages);
struct pmap_node *pmap_alloc(struct pmap_state *state, size_t npages);
int pmap_free(struct pmap_state *state, struct pmap_node *node);

static inline void *pmap_node2va(struct pmap_node *node)
{
    return node->pn_va;
}

void *pmap(void *destva, size_t npages, int perm);
int punmap(void *va, size_t npages);

#endif