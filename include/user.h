#ifndef _USER_H_
#define _USER_H_

#include <nokernel.h>

typedef int gid_t;
typedef int uid_t;

nokernel int chown(const char *pathname, uid_t owner, gid_t group);
nokernel int fchown(int fd, uid_t owner, gid_t group);

#endif