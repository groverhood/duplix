#ifndef _KERNINST_H_
#define _KERNINST_H_

/* A handle to an instance of Duplix running on another node,
   e.g. another core or another machine */
typedef int kerninst_t;

enum { SELFINST };

#endif