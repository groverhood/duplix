#ifndef _BLKDEV_H_
#define _BLKDEV_H_

#include <pmap.h>
#include <stdint.h>
#include <stddef.h>

#define BLKSIZ PGSIZE

struct dev;
typedef uintptr_t blkaddr_t;

int isblkdev(struct dev *dev);
size_t blk_size(struct dev *blkdev);
size_t blk_write(struct dev *blkdev, blkaddr_t ba, void *va, size_t blks);
size_t blk_read(struct dev *blkdev, blkaddr_t ba, void *va, size_t blks);

#endif