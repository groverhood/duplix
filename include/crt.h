#ifndef _CRT_H_
#define _CRT_H_


typedef int(*dtor_t)();
typedef int(*ctor_t)();

extern int __argc;
extern char *__argv[];

extern ctor_t ctors[];
extern dtor_t dtors[];

extern char *process; // = __argv[0]

extern int main();

extern void _start();


#define ctor __attribute__((constructor))
#define ctor1(p) __attribute__((constructor (p)))
#define dtor __attribute__((destructor))
#define dtor1(p) __attribute__((destructor (p)))
#define weaksym __attribute__((weak))

#endif