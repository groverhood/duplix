
include $(MK)/$(ARCH)/flags.mk

LFLAGS += -L$(BUILD)
SFLAGS += -D__ASM__

echo:
	@echo Architecture: $(ARCH)
	@echo C Compiler:   $(CC)
	@echo Linker:       $(LD)

%.o: %.S
	$(AS) $(INCLUDES) $(SFLAGS) -o $@ $<

%.o: %.c
	$(CC) $(INCLUDES) $(CFLAGS) -o $@ $<

%.a: $(OBJS)
	$(AR) $(RFLAGS) $@ $^ 

%.so: $(OBJS)
	$(LD) $(LFLAGS) -shared -o $@ $^

%.bin: $(OBJS)
	$(LD) $(LFLAGS) $^ $(SHARED) -static $(STATIC) -o $(subst .bin,,$@)