LFLAGS += --no-relax -nostdlib -N --script=$(MK)/amd64/kernel.ld
CFLAGS += -D__KERNEL__ -fPIC -mcmodel=medium -Wl,--image-base -Wl,0xffff800000000000 -ffreestanding -mno-red-zone -fno-stack-protector -fno-asynchronous-unwind-tables
INCLUDES += -I$(ROOT)/kernel -I$(ROOT)/kernel/arch/amd64