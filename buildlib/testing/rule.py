import typing as _t
import unittest as _u
import re as _re


class Rule(_u.TestCase):
    _process_name: "str"
    _expected_lines: "list[str]"
    _output_lines: "list[str]"

    @property
    def process_name(self) -> "str":
        return self._process_name

    def __init__(
        self, process_name: "str", expected_lines: "list[str]"
    ) -> None:
        super().__init__(methodName="testOutput")
        self._process_name = process_name
        self._expected_lines = iter(expected_lines)

    def setOutput(self, output: "list[str]") -> None:
        self._output_lines = list(
            output_line.lstrip(f"[{self._process_name}]").lstrip()
            for output_line in output
            if output_line.startswith(f"[{self._process_name}]")
        )

    def testOutput(self) -> None:
        for expected_line, output_line in zip(
            self._expected_lines, self._output_lines
        ):
            self.assertEqual(expected_line, output_line)
