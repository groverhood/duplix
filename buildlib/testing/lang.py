import parsec as _p
import typing as _t
import itertools as _i

from .rule import Rule as _Rule
from .suite import TestSuite as _TestSuite
from ..qemu import QEMU as _QEMU
from ..util import arch as _arch
from ..arch import Arch as _Arch

spaces = _p.regex(r"([ \n\t\r\f]+|#.*\n)*")
identifier = _p.regex(r"[A-Z][A-Z0-9]*")


@_p.generate
def qemu_args() -> "tuple[_t.Optional[_Arch], list[str]]":
    arch_str = yield _p.string("@") >> _p.optional(
        _p.regex(r"[A-Za-z][A-Za-z0-9]+")
    )
    arch = _arch(arch_str) if arch_str is not None else None
    arg_str = yield _p.string(":=") >> _p.many1(_p.none_of("\n"))
    args = "".join(arg_str).split()
    return arch, args


def output_specification(expected_output_id: "str"):
    @_p.generate
    def parser() -> "tuple[str, str, bool]":
        output_id = yield identifier
        if output_id != expected_output_id:
            yield _p.fail_with(
                f"Expected output id '{expected_output_id}', got '{output_id}'"
            )
        output_chars = (
            yield _p.string(">")
            >> _p.many1(_p.none_of("\n\t\r\f>"))
            << _p.string(">")
        )
        output = "".join(output_chars)
        is_eof = yield _p.result(_p.string("EOF"), True)
        return output, is_eof

    return parser


@_p.generate
def output_watcher_declaration() -> "tuple[str, str]":
    output_id = yield identifier
    binary_name = yield _p.string(":=") >> _p.regex(
        r"(?:\.{1,2}|/)(?:(?:[0-9A-Za-z_\-.]|\\ )+/?)*"
    )
    return output_id, binary_name


@_p.generate
def rule() -> "_Rule":
    output_id, binary_name = (
        yield spaces >> output_watcher_declaration << spaces
    )
    outputs = yield _p.many1(
        spaces >> output_specification(output_id) << spaces
    )
    output_lines = [""]
    for output, is_eof in outputs:
        output = output.replace("\\n", "\n")
        output = output.replace("\\t", "\t")
        output = output.replace("\\r", "\r")

        lines = output.splitlines()
        output_lines[-1] += lines[0]
        if len(lines) > 1:
            output_lines.extend(lines[1:])
        if is_eof:
            break

    return _Rule(binary_name, output_lines)


test_script = _p.many(qemu_args) + _p.many1(rule)


def parse_test_script(
    arch: _Arch, filename: "str"
) -> "tuple[_QEMU, _TestSuite]":
    if not filename.endswith(".tst"):
        raise ValueError(".tst file required as input, provided " + filename)

    with open(filename) as testfile:
        args_list, rules = test_script.parse(testfile.read())
        if args_list is None:
            args = []
        else:
            try:
                _, args = next(
                    _i.dropwhile(lambda args: args[0] != arch, args_list)
                )
            except StopIteration:
                raise ValueError(
                    "Test script not intended for arch " + repr(arch)
                )

    qemu = _QEMU(arch, *args)
    testsuite = _TestSuite(filename, rules)
    return qemu, testsuite
