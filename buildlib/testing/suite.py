from typing import Iterator as _Iterator
import unittest as _u
from .rule import Rule as _Rule


class TestSuite(_u.TestSuite):
    _rules: "list[_Rule]"
    _name: "str"

    @property
    def name(self):
        return self._name

    def __init__(self, name: "str", rules: "list[_Rule]") -> "None":
        super().__init__(tests=rules)
        self._name = name
        self._rules = rules

    def __call__(self, output: "str") -> "_u.TestResult":
        output_lines = output.splitlines()
        for rule in self._rules:
            rule.setOutput(output_lines)
        return self.run(_u.TestResult())

    def __iter__(self) -> "_Iterator[_Rule]":
        return iter(self._rules)
