from enum import Enum as _Enum


class Arch(_Enum):
    value: "Object"

    class Object:
        def __init__(self, arch: str):
            prefixes = {"amd64": "x86_64-linux-gnu-"}
            qemus = {"amd64": "qemu-system-x86_64"}
            grubfmts = {"amd64": "x86_64-efi"}
            efiexecs = {"amd64": "grubx64.efi"}
            self.arch = arch
            self.prefix = prefixes[arch]
            self.compiler = self.prefix + "gcc -c"
            self.linker = self.prefix + "ld"
            self.assembler = self.prefix + "gcc -c"
            self.ar = self.prefix + "ar -rc"
            self.qemu = qemus[arch]
            self.grubfmt = grubfmts[arch]
            self.efiexec = efiexecs[arch]
            self.mappings = {
                "ARCH": self.arch,
                "CC": self.compiler,
                "LD": self.linker,
                "AS": self.assembler,
                "QEMU": self.qemu,
                "AR": self.ar,
            }

        def __repr__(self):
            return self.arch

        def __iter__(self):
            return iter(self.mappings)

        def keys(self):
            return self.mappings.keys()

        def __getitem__(self, name: str):
            return self.mappings[name]

    AMD64 = Object("amd64")
