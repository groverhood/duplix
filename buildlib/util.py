import functools as _fn

from typing import Callable as _Callable
from operator import contains as _contains, getitem as _getitem

from .arch import Arch as _Arch


def filter_unwanted_kwargs(f: _Callable):
    @_fn.wraps(f)
    def filter_func(*args, **kwargs):
        varnames = f.__code__.co_varnames
        valid_kwkeys = frozenset(
            filter(_fn.partial(_contains, varnames), kwargs)
        )
        valid_kwargs = dict(
            zip(valid_kwkeys, map(_fn.partial(_getitem, kwargs), valid_kwkeys))
        )
        return f(*args, **valid_kwargs)

    return filter_func


def multikey(val, *keys):
    return {key: val for key in keys}


def arch(arch_str: str):
    enums = multikey(_Arch.AMD64, "amd64", "x64", "x86_64")
    if arch_str not in enums:
        raise ValueError()
    return enums[arch_str]
