from enum import Enum as _Enum


class TextColor(_Enum):
    class Object:
        def __init__(self, color: str):
            self.color = color

        def __call__(self, s: str):
            return f"{self.color}{s}\033[0m"

        def __repr__(self):
            return self("RED")

    def __call__(self, s: str):
        return self.value(s)

    RED = Object("\033[91m")
    GREEN = Object("\033[92m")
