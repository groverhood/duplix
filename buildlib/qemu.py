from .arch import Arch as _Arch
import subprocess as _s


class QEMU:
    _proc: _s.Popen
    _output: "str"

    def __init__(self, arch: _Arch, *args: str) -> None:
        self._proc = _s.Popen([arch.value.qemu, *args], stdout=_s.PIPE)
        self._output = None

    @property
    def output(self) -> "str":
        if self._output is None:
            self._output = self._proc.stdout.read().decode("utf-8")
            self._proc = None
        return self._output
