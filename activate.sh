build() {
    python build.py $@
}

runqemu() {
    docker-compose run emulator
}

if [ ! -d env ]; then 
    python3.9 -m venv env
fi

. env/bin/activate