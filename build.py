from sys import argv
from glob import glob
from collections import defaultdict
from argparse import ArgumentParser
from functools import partial
from itertools import chain, starmap
from os.path import isdir, abspath, isfile
from operator import getitem, methodcaller
from subprocess import run
from shutil import copytree, rmtree
from typing import Iterator, Tuple
from os import listdir, makedirs, getcwd, getenv, rename, unlink
from multiprocessing import cpu_count

import os.path as path

from buildlib.text import TextColor
from buildlib.arch import Arch
from buildlib.testing.lang import parse_test_script
from buildlib.util import *


@filter_unwanted_kwargs
def build(
    *,
    kerndir: "str",
    usrdir: "str",
    libdir: "str",
    outdir: "str",
    makedir: "str",
    arch: "Arch",
):
    def invoke_buildproc(buildloc: "str", fullpath: "str", output: "str"):
        print(TextColor.GREEN(buildloc))
        proc = run(
            ["make", "all", f"-j{cpu_count()}"],
            cwd=fullpath,
            env={
                "PATH": getenv("PATH"),
                "ROOT": getcwd(),
                "BUILD": outdir,
                "LIB": libdir,
                "MK": makedir,
                "SRC": fullpath,
                "OUTPUT": output,
                **arch.value,
            },
        )
        if proc.returncode != 0:
            print(TextColor.RED(f"{buildloc} failed: error {proc.returncode}"))
            exit(1)

    makedirs(outdir, exist_ok=True)
    builds = [
        (lib, f"{libdir}/static/{lib}", f"lib{lib}.a")
        for lib in listdir(libdir + "/static")
    ]
    builds += [("kernel", kerndir, "kernel.bin")]
    builds += [
        (lib, f"{libdir}/{lib}", f"lib{lib}.so")
        for lib in listdir(libdir)
        if lib != "static"
    ]
    builds += [
        (usr, f"{usrdir}/{usr}", f"{usr}.bin") for usr in listdir(usrdir)
    ]
    for build in filter(lambda b: isdir(b[1]), builds):
        invoke_buildproc(*build)


@filter_unwanted_kwargs
def clean(*, kerndir: "str", usrdir: "str", libdir: "str", outdir: str):
    cleantargets = chain.from_iterable(
        glob(f"{dir}/**/*.{ext}", recursive=True)
        for dir in (kerndir, usrdir, libdir)
        for ext in ("o", "so", "a")
    )
    for target in cleantargets:
        unlink(target)
    rmtree(outdir, ignore_errors=True)


def generate_grub(
    outdir: "str", bringup: "str | None" = None
) -> "Iterator[Tuple[str, str]]":
    def endswith(suffixes: "Tuple[str, ...]", s: "str"):
        return str.endswith(s, suffixes)

    objects = [
        object
        for object in listdir(outdir)
        if isfile(path.join(outdir, object))
    ]
    libsuffixes = (".a", ".so")
    dirmap = ("/bin/", "/lib/")
    cmdmap = defaultdict(partial(str, "module2"))
    object_suffixes = map(partial(endswith, libsuffixes), objects)
    object_dirs = map(partial(getitem, dirmap), object_suffixes)
    object_paths = list(starmap(path.join, zip(object_dirs, objects)))
    object_commands = zip(map(partial(getitem, cmdmap), objects), object_paths)
    multiboot_command = "multiboot2 /bin/kernel"
    if bringup is not None:
        multiboot_command += " bringup=" + bringup + " shutdown"

    print(multiboot_command)
    commands = chain(
        [multiboot_command, "insmod efi_gop", "insmod efi_uga"],
        map(" ".join, object_commands),
        ["boot"],
    )
    header = "set default=0;\nset timeout=0;\n"
    menuentry = 'menuentry "Duplix" {\n    ' + "\n    ".join(commands) + "\n}"
    grubpath = path.join(outdir, "root/boot/grub")
    makedirs(grubpath, exist_ok=True)
    with open(path.join(grubpath, "grub.cfg"), "w+") as grubcfg:
        grubcfg.write(header + menuentry)
    return zip(objects, object_paths)

@filter_unwanted_kwargs
def mkbootiso(outdir: "str", arch: Arch):
    makedirs(path.join(outdir, "root/boot/efi/EFI/"), exist_ok=True)
    copytree(
        f"/usr/lib/grub/{arch.value.grubfmt}",
        path.join(outdir, "root/grub", arch.value.grubfmt),
        dirs_exist_ok=True,
    )
    makedirs(path.join(outdir, "root/share/grub"), exist_ok=True)
    run(
        [
            "grub-mkimage",
            f'--prefix={path.join(outdir, "root")}',
            f"--format={arch.value.grubfmt}",
            f'--output={path.join(outdir, "root/boot/efi/EFI", arch.value.efiexec)}',
        ]
    )
    run(
        [
            "grub-mkfont",
            "-o",
            path.join(outdir, "root/share/grub/unicode.pf2"),
            "/usr/share/fonts/truetype/unifont/unifont.ttf",
        ]
    )
    run(
        [
            "grub-mkrescue",
            f'--output={path.join(outdir, "duplix.iso")}',
            path.join(outdir, "root"),
        ]
    )


def run_tests(arch: "Arch", tests: "list[str]", **kwds) -> "None":
    test_suites = map(
        partial(parse_test_script, arch),
        map(lambda test: path.join("tests", test + ".tst"), tests),
    )
    for qemu, testsuite in test_suites:
        kwds["bringup"] = ",".join(rule.process_name for rule in testsuite)
        build(arch=arch, **kwds)
        outdir = kwds["outdir"]
        for object, dirmapping in generate_grub(outdir, kwds["bringup"]):
            rename(f"{outdir}/{object}", f"{outdir}/root{dirmapping}")
        mkbootiso(arch=arch, **kwds)
        testsuite(qemu.output)


def main() -> "None":
    ap = ArgumentParser(
        prog="build.py",
        usage="python3 build.py [OPTIONS...]",
        description="Build script for Duplix",
    )
    ap.add_argument(
        "--kerndir",
        "-k",
        type=abspath,
        default="kernel",
        help="the directory containing the kernel sources",
        dest="kerndir",
    )
    ap.add_argument(
        "--usrdir",
        "-u",
        type=abspath,
        default="user",
        help="the directory containing the user binary sources",
        dest="usrdir",
    )
    ap.add_argument(
        "--libdir",
        "-l",
        type=abspath,
        default="lib",
        help="directory containing libraries for use in linking binaries",
        dest="libdir",
    )
    ap.add_argument(
        "--outputdir",
        "-o",
        type=abspath,
        default="build",
        help="directory to output binaries in",
        dest="outdir",
    )
    ap.add_argument(
        "--makedir",
        "-m",
        type=abspath,
        default="mk",
        help="directory containing make sources",
        dest="makedir",
    )
    ap.add_argument(
        "--arch",
        "-a",
        required=False,
        default=Arch.AMD64,
        type=arch,
        help="the architecture to build for",
        dest="arch",
    )
    ap.add_argument(
        "--clean",
        "-C",
        required=False,
        action="store_true",
        help="clean up the intermediate build files and outputted binaries",
        dest="clean",
    )
    tests = list(map(methodcaller("rstrip", ".tst"), listdir("tests")))
    ap.add_argument(
        "--run-tests",
        "-T",
        required=False,
        default=None,
        choices=tests,
        nargs="+",
        help="run select tests in the tests directory: " + ", ".join(tests),
        dest="tests",
    )

    args = ap.parse_args(argv[1:])
    if args.clean:
        clean(**vars(args))
    elif args.tests is not None:
        run_tests(**vars(args))
    else:
        build(**vars(args))
        makedirs(f"{args.outdir}/root/lib", exist_ok=True)
        makedirs(f"{args.outdir}/root/bin", exist_ok=True)
        for object, dirmapping in generate_grub(args.outdir):
            rename(f"{args.outdir}/{object}", f"{args.outdir}/root{dirmapping}")
        mkbootiso(args.outdir, args.arch)


if __name__ == "__main__":
    main()
